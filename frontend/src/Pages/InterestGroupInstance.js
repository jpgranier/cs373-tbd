import React from 'react';
import {Nav, Container, Col, Row, Image, Table} from 'react-bootstrap'
import InterestGroupCard from '../Components/InterestGroupCard';
import CandidateRating from '../Components/IG_CandidateRatings';
import BillsByCategory from '../Components/IG_BillsByCategory';
import InterestGroupGraph from '../Components/InterestGroupGraph'
import SIG from '../Assets/SIG.jpg'
import '../Styles/InterestGroupInstance.css'
class InterestGroupInstance extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        interest : [],
        ratings : [],
        logo : null,
        isLoaded : null,
      }
  }

  componentDidMount() {
    let url = 'https://api.thefundsdontstop.me'
    let url2 = 'http://localhost:5000'
    
    fetch(url + '/getInterestGroupsByVotesmartId?votesmart_id=' + this.props.match.params.votesmart_id)
    .then(res => res.json())
    .then(data => {
        this.setState({interest: data}) //fill
        this.setState({logo: data.logo_url})
    })
    fetch(url + '/getRecentRatings?sig_id=' + this.props.match.params.votesmart_id) 
    .then(res => res.json())
    .then(data => {
        this.setState({ratings: data}) //fill
        this.setState({isLoaded: true})
    })
    .catch(console.log)

  }



  render() {

    if (this.state.isLoaded === null){
        return (<div>loading...</div>)
    }

    if (this.state.logo === null){
      this.setState({logo: SIG})
    }

    return (
      <Container>
        <p>**image is not guaranteed to be representative of this interest group</p>
        <Image src={this.state.logo} height="100px"/>
        <Row>
          <Col><h1>{ this.state.interest.name }</h1></Col>
        </Row>
        <Row>
          <Col>
            <InterestGroupCard interest={this.state.interest} />
          </Col>
          <Col>
            <InterestGroupGraph interest={this.state.interest} />
          </Col>
        </Row>
        <br></br>
        <Row>
          <Col>
            <h3>Bills of Interest</h3>
              <BillsByCategory category={this.state.interest.category_id} />
          </Col>
          <Col>
            <h3>Candidate Ratings</h3>
              <CandidateRating ratings={this.state.ratings} />
          </Col>
        </Row>

      </Container>
    );
  }
}

export default InterestGroupInstance;
