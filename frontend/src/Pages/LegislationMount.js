import React, { Component } from 'react';
import LegislationInstance from './LegislationInstance';
class LegislationMount extends Component{
    constructor(props){
        super(props);
        this.state = {
            votesmart_bills : null,
            sigList : null,
            candidateNames : null,
            sigIsLoaded : null,
            candidateDictIsLoaded : null,
            partyIsLoaded: null,
            unpackedActionIds : null,
            candidateParties : null
        }
    }

    componentDidMount(){
        //need to "chain" multiple promises together
        //first fetch from /getVotesmartBill
        fetch("https://api.thefundsdontstop.me/getVotesmartBill?bill_id="+ this.props.match.params.bill_id)
        .then((res) => {
            return res.json()})
        .then(data => {
            this.setState({votesmart_bills : data});
            var uai = data.actionIds.split("_");
            this.setState({unpackedActionIds : uai});
            var billId = data['bill_id'];
            var categoryId = data['categories'];
            return [billId, categoryId];
        })
        .then(data =>  {
            return fetch("https://api.thefundsdontstop.me/getInterestGroupsByCategoryId?category_id=" + data[1])}
        )
        .then(data => {
            return data.json()})
        .then(data =>{
            this.setState({sigList: data}); 
            this.setState({sigIsLoaded : true}) 
        })
        .then(() => {
            return fetch("https://api.thefundsdontstop.me/getCandidateIds")
        }) //relating votesmart_id with candidate name to make life easy
        .then(data => data.json())
        .then(candidateDicts => {
            var candMap = {}
            candidateDicts.forEach((candidateDict) => {
                candMap[candidateDict["candidate_votesmart_id"]] = candidateDict["candidate_name"];
            });
            this.setState({candidateNames : candMap});
            this.setState({candidateDictIsLoaded : true})
        })
        .then(() => {
            return fetch("https://api.thefundsdontstop.me/getAllCandidateInfo")})
        .then(data => data.json())
        .then(data => {
            var partyMap = {}
            data.forEach((candidate) => {
                partyMap[candidate.votesmart_id] = candidate.party
            })
            this.setState({candidateParties : partyMap})
            this.setState({partyIsLoaded : true})
            
        }).catch(console.log);
    }

    render(){
        if(this.state.partyIsLoaded === null || 
            this.state.candidateDictIsLoaded === null || 
            this.state.sigIsLoaded === null){
            return(
                <p> 
                    ... Loading ...
                </p>
            );
        }
        return (
            <div>
                <LegislationInstance    votesmart_bills={this.state.votesmart_bills} 
                                        sigList={this.state.sigList} 
                                        candidateNames={this.state.candidateNames}
                                        unpackedActionIds={this.state.unpackedActionIds}
                                        candidateParties={this.state.candidateParties}/>
                                       
            </div>
        );
    }
}

export default LegislationMount;