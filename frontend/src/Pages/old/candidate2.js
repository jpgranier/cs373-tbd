import React from 'react';
import { Fragment } from 'react';
import ez from '../ez.jpg'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

export class candidate2 extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Fragment>
      <h1> Elizabeth Warren : Democrat </h1>
      <img src = {ez} alt="ez" />
      <p> District: D-MA </p>
      <p> Top contributor: Alphabet Inc</p>
      <a href= {"https://justfacts.votesmart.org/candidate/campaign-finance/141272/elizabeth-warren"}> Source </a>
      <p> Important issues: </p>
      <p> Polls </p>
      <a href= {"https://www.realclearpolitics.com/epolls/2020/president/us/2020_democratic_presidential_nomination-6730.html"}> Click here </a>
      <p> Positions </p>
      <a href= {"https://justfacts.votesmart.org/candidate/political-courage-test/141272/elizabeth-warren"}> Click here </a>
      <p> Elections participated: </p>
      <a href= {"https://en.wikipedia.org/wiki/Electoral_history_of_Elizabeth_Warren"}> Click here </a>

      </Fragment>
  }
}