import React from 'react';
import { Fragment } from 'react';
import bern from '../bern.jpg'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

export class candidate1 extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Fragment>
      {/* <Navbar color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/components/">Components</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Options
                </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Option 1
                  </DropdownItem>
                <DropdownItem>
                  Option 2
                  </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Reset
                  </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar> */}
      <h1> Bernie Sanders : Democrat</h1>
      <img src = {bern} alt="bern" />
      <p> District: D-VT </p>
      <p> Top contributor: University of California </p>
      <a href= {"https://justfacts.votesmart.org/candidate/campaign-finance/27110/bernie-sanders"}> Source </a>
      <p> Important issues: </p>
      <p> Polls </p>
      <a href= {"https://www.realclearpolitics.com/epolls/2020/president/us/2020_democratic_presidential_nomination-6730.html"}> Click here </a>
      <p> Positions </p>
      <a href= {"https://justfacts.votesmart.org/candidate/political-courage-test/27110/bernie-sanders"}> Click here </a>
      <p> Elections participated: </p>
      <a href= {"https://en.wikipedia.org/wiki/Electoral_history_of_Bernie_Sanders"}> Click here </a>

      </Fragment>
  }
}