import React, { Component } from 'react';
import {Container, Row, Col, Image, Button, Table, Card, Dropdown, DropdownButton, ButtonGroup} from 'react-bootstrap';
import ModelPagination from "../Components/ModelPagination"
import styles from './highlighter.css'
import Highlighter from "react-highlight-words";
const url = "https://api.thefundsdontstop.me/getInterestGroupsRangeFilterSearchAndSort?offset="
const filter = false
const perPage = 10

export default class interests extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         loaded: false,
         Interests: [],
         size: -1,
         offset: 1,
         url : "",
         parameters: "",
         name_first_letter: "",
         total_contributions_start: "",
         total_contributions_end: "",
         dem_contributions_start: "",
         dem_contributions_end: "",
         repub_contributions_start: "",
         repub_contributions_end: "",
         pac_contributions_start: "",
         pac_contributions_end: "",
         search: "",
         sort_column: "",
         asc_or_desc: "",
         first_letter: "",
         contribution: "A", //A, T, R, D, P, C
         formVal: ""
      }

      this.state.parameters = this.state.name_first_letter + this.state.total_contributions_start + this.state.total_contributions_end + this.state.dem_contributions_start + this.state.dem_contributions_end+ this.state.repub_contributions_start + this.state.repub_contributions_end + this.state.pac_contributions_start +this.state.pac_contributions_end + this.state.search + this.state.sort_column + this.state.asc_or_desc
      this.state.url = url+this.state.offset+this.state.parameters
      
      this.changePage = this.changePage.bind(this);
      this.updateParameters = this.updateParameters.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.firstPage = this.firstPage.bind(this);
      this.previousPage = this.previousPage.bind(this);
      this.nextPage = this.nextPage.bind(this);
      this.lastPage = this.lastPage.bind(this);
      this.selectPage = this.selectPage.bind(this);
      this.prevEll = this.prevEll.bind(this);
      this.nextEll = this.nextEll.bind(this);
      this.resetFilters = this.resetFilters.bind(this);
   }
   

   

   
   renderTableData() {
      let rows = this.state.Interests.map((Interest) => {
         let total = Interest.total
         let dems = Interest.dems
         let repubs = Interest.repubs
         let gave_to_pac = Interest.gave_to_pac
         let gave_to_cand = Interest.gave_to_cand
         if (Interest.total == -1){total = 'N/A';}
         if (Interest.dems == -1) {dems = 'N/A';}
         if (Interest.repubs == -1) {repubs = 'N/A';}
         if (Interest.gave_to_pac == -1) {gave_to_pac = 'N/A';}
         if (Interest.gave_to_cand == -1) {gave_to_cand = 'N/A';}
         return (
            <tr>
               <th scope="row"><a href={'interest_groups/'+Interest.votesmart_id}>
                  {<Highlighter
                  searchWords={[this.state.formVal]}
                  textToHighlight={Interest.name}
                  highlightStyle={{ fontWeight: 'bold' }}/>}</a></th>
               <td scope="col" class = "text-center">{"$" + total}</td>
               <td scope="col" class = "text-center">{"$" + dems}</td>
               <td scope="col" class = "text-center">{"$" + repubs}</td>
               <td scope="col" class = "text-center">{"$" + gave_to_pac}</td>
               
            </tr>
         )
      })
      return rows
 
   }

   async changePage(offset) {
      this.setState({loaded: false, offset : offset})
      this.state.url = url+offset+this.state.parameters
      const response = await fetch(this.state.url)
      const data = await response.json();
      this.setState({loaded: true, Interests: data.data, size: Math.ceil(data.size/perPage)})
   }
   
   async updateParameters() {
      const offset = 1
      this.state.parameters = this.state.name_first_letter + this.state.total_contributions_start + this.state.total_contributions_end + this.state.dem_contributions_start + this.state.dem_contributions_end+ this.state.repub_contributions_start + this.state.repub_contributions_end + this.state.pac_contributions_start +this.state.pac_contributions_end + this.state.search + this.state.sort_column + this.state.asc_or_desc
      this.state.url = url+offset+this.state.parameters
   
      this.setState({loaded : false})
         const response = await fetch(this.state.url)
         const data = await response.json();
         this.setState({loaded: true, Interests: data.data, size: Math.ceil(data.size/perPage), offset: 1})
   
   }


  
   async nameSortAsce() {
      if (this.state.sort_column == "&sort_column=name" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=name" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters() 
   }

   async nameSortDesc() {
      if (this.state.sort_column == "&sort_column=name" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=name" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters() 
   }
   
   async t_contributionSortAsce() {
      if (this.state.sort_column == "&sort_column=total_contributions" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=total_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters() 
   }

   async t_contributionSortDesc() {
      if (this.state.sort_column == "&sort_column=total_contributions" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=total_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async d_contributionSortAsce() {
      if (this.state.sort_column == "&sort_column=dem_contributions" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=dem_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async d_contributionSortDesc() {
      if (this.state.sort_column == "&sort_column=dem_contributions" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=dem_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async r_contributionSortAsce() {
      if (this.state.sort_column == "&sort_column=repub_contributions" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=repub_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async r_contributionSortDesc() {
      if (this.state.sort_column == "&sort_column=repub_contributions" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=repub_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async p_contributionSortAsce() {
      if (this.state.sort_column == "&sort_column=pac_contributions" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=pac_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async p_contributionSortDesc() {
      if (this.state.sort_column == "&sort_column=pac_contributions" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=pac_contributions" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async nameFilter(letter) {
      if (this.state.name_first_letter == "&name_first_letter=" + letter) {
         this.state.name_first_letter = ""
      }
      else {
         this.state.name_first_letter = "&name_first_letter=" + letter
         this.state.offset = 1
      }
      this.updateParameters()
   }
   

   async republicanFilter(range) {
      if (this.state.repub_contributions_start == "&repub_contributions_start=" +  range[0] && this.state.repub_contributions_end == "&repub_contributions_end=" +  range[1]) {
         this.state.repub_contributions_end = ""
         this.state.repub_contributions_start = ""
      }
      else {
         this.state.repub_contributions_start = "&repub_contributions_start=" +  range[0]
         this.state.repub_contributions_end = "&repub_contributions_end=" +  range[1]
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async democraticFilter(range) {
      if (this.state.dem_contributions_start == "&dem_contributions_start=" +  range[0] && this.state.dem_contributions_end == "&dem_contributions_end=" +  range[1]) {
         this.state.dem_contributions_end = ""
         this.state.dem_contributions_start = ""
      }
      else {
         this.state.dem_contributions_start = "&dem_contributions_start=" +  range[0]
         this.state.dem_contributions_end = "&dem_contributions_end=" +  range[1]
         this.state.offset = 1
      }
      this.updateParameters()
   }
   async totalFilter(range) {
      if (this.state.total_contributions_start == "&total_contributions_start=" +  range[0] && this.state.total_contributions_end == "&total_contributions_end=" +  range[1]) {
         this.state.total_contributions_end = ""
         this.state.total_contributions_start = ""
      }
      else {
         this.state.total_contributions_start = "&total_contributions_start=" +  range[0]
         this.state.total_contributions_end = "&total_contributions_end=" +  range[1]
         this.state.offset = 1
      }
      this.updateParameters()
   }
   async pacFilter(range) {
      if (this.state.pac_contributions_start == "&pac_contributions_start=" +  range[0] && this.state.pac_contributions_end == "&pac_contributions_end=" +  range[1]) {
         this.state.pac_contributions_end = ""
         this.state.pac_contributions_start = ""
      }
      else {
         this.state.pac_contributions_start = "&pac_contributions_start=" +  range[0]
         this.state.pac_contributions_end = "&pac_contributions_end=" +  range[1]
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async componentDidMount() {
      const response = await fetch(this.state.url);
      const data = await response.json();
      this.setState({loaded: true, Interests: data.data, size: Math.ceil(data.size/perPage)});
      console.log(data.data);
      
   }

   renderSortingButtonsUpDown(asce_f, desc_f, t){
      return(
         <span>
            <button type="button" class="btn btn-success btn-sm" onClick={()=> {asce_f.call(t)}}>
               {'↑'}    
            </button>
            <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
               {'↓'}
            </button>
         </span>
      )
   }

   renderSortingButtonsCategory(asce_f, desc_f, text1, text2, t){
      return(
         <span>
            <button type="button" class="btn btn-primary btn-sm" onClick={()=> {asce_f.call(t)}}>
               {text1}
            </button>
            <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
               {text2}
            </button>
         </span>
      )
   }

   renderSingleFuncDropdown(title, items, func, t){
      return(
         <DropdownButton as={ButtonGroup} drop='down' variant='info' size='lg' id="dropdown-basic-button" title={title}>
            <div style={{maxHeight: '300px', overflow: 'scroll'}}>
               {items.map((item_title, index) => {
                  if (item_title.length == 1){
                     return(
                        <Dropdown.Item onClick={() => {func.call(t, item_title)}}>
                           {item_title}
                        </Dropdown.Item>
                     );
                  }
                  else{
                     if(item_title[1]== 999999999){
                        return(
                           <Dropdown.Item onClick={() => {func.call(t, item_title)}}>
                             >= ${item_title[0]}
                           </Dropdown.Item>
                        );   
                     }
                     else{
                        return(
                           <Dropdown.Item onClick={() => {func.call(t, item_title)}}>
                              ${item_title[0]} - ${item_title[1]}
                           </Dropdown.Item>
                        );   

                     }

                  }

               
               })}
            </div>
         </DropdownButton>
      )
   }

   renderDropdown(title, items, funcs, t){
      return(
         <DropdownButton as={ButtonGroup} drop='down' variant='info' size='lg' id="dropdown-basic-button" title={title}>
               {items.map((item_title, index) => {
                  return(
                     <Dropdown.Item onClick={() => {funcs[index].call(t)}}>
                        {item_title}
                     </Dropdown.Item>
                  );
               
               })}
         </DropdownButton>
      )
   }


   // pagination methods ---------
   async firstPage() {
      const offset2 = 1
      this.changePage(offset2)
   }
   
   async prevEll(){
      const offset2 = Math.max(this.state.offset - 3, 1)
      this.changePage(offset2)
   }
   
   async previousPage() {
      const offset2 = this.state.offset - 1
      this.changePage(offset2)
   }
   
   async nextPage() {
      const offset2 = this.state.offset + 1
      this.changePage(offset2)
   }
   
   async nextEll(){
      const offset2 = Math.min(this.state.size, this.state.offset + 3)
      this.changePage(offset2)
   }
   
   async lastPage() {
      const offset2 = this.state.size
      this.changePage(offset2)
   }
   
   async selectPage(index){
      const offset2 = index
      this.changePage(offset2)
   }

   renderButtons() {

      let rows = []

      rows.push(
         <ModelPagination  
            size={this.state.size}
            current={this.state.offset}
            selectPage={this.selectPage}
            prev={this.previousPage}
            next={this.nextPage}
            first={this.firstPage}
            last={this.lastPage}
            nextEll={this.nextEll}
            prevEll={this.prevEll}
         />
      )

      const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
      const ranges = [[0, 1000], [1001,10000], [10001, 100000], [100001, 1000000], [1000001, 999999999]]

      rows.push(
         <React.Fragment>
            <tr style={{fontSize: '22px'}}>
               <th scope="col" class="text-center" >
                  {this.renderSingleFuncDropdown("Name", letters, this.nameFilter, this)}
                  <br/>
                  {this.renderSortingButtonsCategory(this.nameSortAsce, this.nameSortDesc, 'A-Z', 'Z-A', this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("Total $", ranges, this.totalFilter, this)} 
                  <br/>
                  {this.renderSortingButtonsUpDown(this.t_contributionSortAsce, this.t_contributionSortDesc, this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("D Contribution", ranges, this.democraticFilter, this)}
                  <br/>
                  {this.renderSortingButtonsUpDown(this.d_contributionSortAsce, this.d_contributionSortDesc, this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("R Contribution", ranges, this.republicanFilter, this)}
                  <br/>
                  {this.renderSortingButtonsUpDown(this.r_contributionSortAsce, this.r_contributionSortDesc, this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("PAC Contribution", ranges, this.pacFilter, this)}
                  <br/>
                  {this.renderSortingButtonsUpDown(this.p_contributionSortAsce, this.p_contributionSortDesc, this)}
               </th>
            </tr>

         </React.Fragment>
      )

     

      return(
         <thead>
            {rows}
         </thead>
      )



      if (this.state.offset - 1 < 1) {
         return (
            <thead>
         <button onClick={() => this.nextPage()}>{"Next"}</button>
         <button onClick={() => this.nextPage()}>{">>"}</button>
         
         <tr>
         <th scope="col">Photo</th>
      <th scope = "col" onClick={() => this.firstNameSort()}>First Name</th>
      <th scope="col" onClick={() => this.lastNameSort()}>Last Name</th>
      <th scope="col" onClick={() => this.partyFilter()}>Party</th>
      <th scope="col" onClick={() => this.democratFilter()}>District</th>
      <th scope="col" onClick={() => this.officeFilter()}>Office</th>
      {/* <th scope="col" onClick={() => this.houseFilter()}>{"House"}</th> */}
      </tr>
      </thead>)
      }
      return (
         <thead>
      <button onClick={() => this.previousPage()}>{"<<"}</button>
      <button onClick={() => this.previousPage()}>{"Back"}</button>
      <button onClick={() => this.nextPage()}>{"Next"}</button>
      <button onClick={() => this.nextPage()}>{">>"}</button>
      <tr>
      <th scope="col">Photo</th>
      <th scope="col" onClick={() => this.firstNameSort()}>First Name</th>
      <th scope="col" onClick={() => this.lastNameSort()}>Last Name</th>
      <th scope="col"onClick={() => this.partyFilter()}>Party</th>
      <th scope="col"onClick={() => this.democratFilter()}>District</th>
      <th scope="col"onClick={() => this.officeFilter()}>Office</th>
      {/* <th scope="col"onClick={() => this.houseFilter()}>{"House"}</th> */}
      </tr>
      </thead>)
   }

   handleChange(event){
      this.state.formVal = event.target.value
      //this.setState({formVal:event.target.value})
      if (this.state.formVal == "") {
         this.state.search = ""
         this.updateParameters()
      }
      else {
         this.search()
      }
     
      this.setState({loaded: true})
   }
   
   handleSubmit(event) {
      //this.setState({formVal: event.target.value});
      if (this.state.formVal == "") {
         this.state.search = ""
         this.updateParameters()
      }
      else {
         this.setState({loaded:false})
         this.search()
         this.setState({loaded:true})
      }
   
      event.preventDefault();
   }
   
   
   async search() {
      this.state.search = "&search=" + this.state.formVal.replace(/\s+/g, '')
      this.updateParameters()
   }

   resetFilters() {
      let newstate = { 
         loaded: false,
         Interests: [],
         size: -1,
         offset: 1,
         url : "",
         parameters: "",
         name_first_letter: "",
         total_contributions_start: "",
         total_contributions_end: "",
         dem_contributions_start: "",
         dem_contributions_end: "",
         repub_contributions_start: "",
         repub_contributions_end: "",
         pac_contributions_start: "",
         pac_contributions_end: "",
         search: "",
         sort_column: "",
         asc_or_desc: "",
         first_letter: "",
         contribution: "A", //A, T, R, D, P, C
         formVal: ""
      }
      this.state = newstate
      this.setState(newstate)
      this.updateParameters()

   }


   render() {

      if (!this.state.loaded) {
          return (<div>Loading...</div>)
       }

      
    return (
        <div>
            
            <button type="button" class="btn btn-primary btn-sm" onClick={()=> {this.resetFilters.call()}}>
               {'Reset Filters'}      
            </button>
           <h1 id='title'>Interest Groups</h1>
         <h4>Page: {this.state.offset}/{this.state.size}</h4>
           <form onSubmit={this.handleSubmit}>
        <label>
          <input type="text" value={this.state.formVal} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search" />
      </form>
           <table class='table'>
           
           {this.renderButtons()}
        

      
              <tbody> 
              {this.renderTableData()}
              
               </tbody>
           </table>

           
           
        </div>
     )
   }

}

