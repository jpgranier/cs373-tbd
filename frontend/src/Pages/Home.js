import React from 'react';
import pennybags from '../Assets/pennybags.png'
import capitol from '../Assets/capitol.png'
import candidate from '../Assets/candidate.jpg'
import styled from 'styled-components';
import money from '../Assets/money.jpg';
import {Container, Row, Col, Image, Button, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import {Banner} from '../Components/Banner';

class Home extends React.Component {

  render() {

    return (
        <React.Fragment>
        <Banner/>
        <Container>

        
        <div>
        <center><h1>Welcome to The Funds Don't Stop</h1></center>
            <br></br>
            <br></br>
            <h3>This website tracks the legislative process with a direct emphasis on lobbying and campaign
                finance. Explore candidates, issues, and interest groups. See how they are connected through issue stances, endorsements, and
                campaign contributions.</h3>


        </div>

        <Row className="show-grid text-center">

        <Col xs={12} sm={4}>
        <Image src={pennybags} style={{height: 300}}/>
        </Col>

        <Col xs={12} sm={4}>
        <Image src={candidate} style={{height: 300}}/>
        </Col>

        <Col xs={12} sm={4}>
        <Image src={capitol} style={{height: 300}} />
        </Col>

        <Col xs={12} sm={4}>
        <Link to="/interests">
          <button type="button" class="btn btn-primary btn-lg">
               Interest Groups</button>
        </Link>
        </Col>

        <Col xs={12} sm={4}>
        <Link to="/candidates">
          <button type="button" class="btn btn-primary btn-lg">
               Candidates</button>
        </Link>
        </Col>

        <Col xs={12} sm={4}>
        <Link to="/legislation">
          <button type="button" class="btn btn-primary btn-lg">
               Legislation</button>
        </Link>
        </Col>

        </Row>

        </Container>

    </React.Fragment>
    );
  }
}

export default Home;

