import React, { Component } from 'react';
import {Container, Row, Col, Image, Button, Table, Card, Dropdown, DropdownButton, ButtonGroup} from 'react-bootstrap';
import Highlighter from "react-highlight-words";
import styles from './highlighter.css'
import ModelPagination from "../Components/ModelPagination"
const url = "https://api.thefundsdontstop.me/getCandidateInfoRangeFilterSearchAndSort?offset="
const filter = false
const perPage = 10


export default class Candidates extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         loaded: false,
         Candidates: [],
         size: -1,
         offset: 1,
         first_name: "",
         last_name: "",
         party: "",
         office: "",
         search: "",
         State:  "",
         sort_column: "",
         asc_or_desc: "",
         formVal: "",
         state_list : null,
         parameters: "",
         url : ""
      }
      this.state.parameters = this.state.first_name+this.state.last_name+this.state.party+this.state.office+this.state.search+this.state.sort_column+this.state.asc_or_desc+this.state.State
      this.state.url = url+this.state.offset+this.state.parameters
      
      this.changePage = this.changePage.bind(this);
      this.updateParameters = this.updateParameters.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.firstPage = this.firstPage.bind(this);
      this.previousPage = this.previousPage.bind(this);
      this.nextPage = this.nextPage.bind(this);
      this.lastPage = this.lastPage.bind(this);
      this.selectPage = this.selectPage.bind(this);
      this.prevEll = this.prevEll.bind(this);
      this.nextEll = this.nextEll.bind(this);
      this.resetFilters = this.resetFilters.bind(this);
   }
   

   
  
   renderTableData() {
      return this.state.Candidates.map((Candidate) => {
            if (Candidate.district != null) {
               Candidate.state = Candidate.state + " " + Candidate.district
               Candidate.district = null
            }
            
      return (
         <Card style={{width:"20%", height:"20%"}}>
            <Card.Img src={Candidate.image_url} variant="top" />
               <Card.Body>
                  <Card.Title>
                     <a href={ "candidate/" + Candidate.votesmart_id}>
                        {<Highlighter
                           searchWords={[this.state.formVal]}
                           textToHighlight={Candidate.first_name + " " + Candidate.last_name}
                           highlightStyle={{ fontWeight: 'bold' }}/>}
                     </a>
                  </Card.Title>
                  <Card.Text>
                     <p>
                        Party:
                        {<Highlighter
                           searchWords={[this.state.formVal]}
                           textToHighlight={Candidate.party}
                           highlightStyle={{ fontWeight: 'bold' }}/>}
                        <br></br>
                        State/District: 
                        {<Highlighter
                           searchWords={[this.state.formVal]}
                           textToHighlight={Candidate.state}
                           highlightStyle={{ fontWeight: 'bold' }}/>}
                        <br></br>
                        Office: 
                        {<Highlighter
                           searchWords={[this.state.formVal]}
                           textToHighlight={Candidate.office}
                           highlightStyle={{ fontWeight: 'bold' }}/>}
                     </p>
                  </Card.Text> 
               </Card.Body>
         </Card>
         )
      })
   }


   

   // Sorts ------------------
   async lastNameSortAsce() {
      if (this.state.sort_column == "&sort_column=last_name" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=last_name" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()      

   }

   async lastNameSortDesc() {
      if (this.state.sort_column == "&sort_column=last_name" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=last_name" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }
   
   async stateSortAsce() {
      if (this.state.sort_column == "&sort_column=state" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=state" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }


   async stateSortDesc() {
      if (this.state.sort_column == "&sort_column=state" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=state" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async firstNameSortAsce() {
      if (this.state.sort_column == "&sort_column=first_name" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=first_name" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async firstNameSortDesc() {
      if (this.state.sort_column == "&sort_column=first_name" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=first_name" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async democratSort() {
      if (this.state.sort_column == "&sort_column=party" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=party" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
      
   }

   async republicanSort() {
      if (this.state.sort_column == "&sort_column=party" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=party" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()

   }

   async senateSort() {
      if (this.state.sort_column == "&sort_column=office" && this.state.asc_or_desc == "&asc_or_desc=desc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=office" 
         this.state.asc_or_desc = "&asc_or_desc=desc"
         this.state.offset = 1
      }
      this.updateParameters()
   }
   async houseSort() {
      if (this.state.sort_column == "&sort_column=office" && this.state.asc_or_desc == "&asc_or_desc=asc") {
         this.state.sort_column = ""
         this.state.asc_or_desc = ""
      }
      else {
         this.state.sort_column = "&sort_column=office" 
         this.state.asc_or_desc = "&asc_or_desc=asc"
         this.state.offset = 1
      }
      this.updateParameters()
   }

   // Filters ------------------

   async stateFilter(state) {
      if (this.state.State == "&state=" + state) {
         this.state.State = ""
      }
      else {
         this.state.State = "&state=" + state
         this.state.offset = 1
      }
      this.updateParameters()
   }
   

   

   async republicanFilter() {
      if (this.state.party == "&party=R") {
         this.state.party = ""
      }
      else {
         this.state.party = "&party=R"
         this.state.offset = 1
      }
      this.updateParameters()   }

   async democratFilter() {
      if (this.state.party == "&party=D") {
         this.state.party = ""
      }
      else {
         this.state.party = "&party=D"
         this.state.offset = 1
      }
      this.updateParameters()   }

   async senateFilter() {
      if (this.state.office == "&office=Senate") {
         this.state.office = ""
      }
      else {
         this.state.office = "&office=Senate"
         this.state.offset = 1
      }
      this.updateParameters()   }
   async houseFilter() {
      if (this.state.office == "&office=House") {
         this.state.office = ""
      }
      else {
         this.state.office = "&office=House"
         this.state.offset = 1
      }
      this.updateParameters()   }

   async firstNameFilter(letter) {
      if (this.state.first_name == "&first_name_first_letter=" + letter) {
         this.state.first_name = ""
      }
      else {
         this.state.first_name = "&first_name_first_letter=" + letter
         this.state.offset = 1
      }
      this.updateParameters()
   }

   async lastNameFilter(letter) {
      if (this.state.last_name == "&last_name_first_letter=" + letter) {
         this.state.last_name = ""
      }
      else {
         this.state.last_name = "&last_name_first_letter=" + letter
         this.state.offset = 1
      }
      this.updateParameters()
   }
   

   async componentDidMount() {
      const response = await fetch(this.state.url);
      const data = await response.json();
      const state_response = await fetch("https://api.thefundsdontstop.me/getAllCandidateInfoStates");
      const data_response = await state_response.json();
      this.setState({loaded: true, Candidates: data.data, size: Math.ceil(data.size/perPage), state_list: data_response});      
   }
   
   
   renderSortingButtonsUpDown(asce_f, desc_f, t){
      return(
         <span style={{fontSize: "20px"}}>
            <button type="button" class="btn btn-success btn-sm" onClick={()=> {asce_f.call(t)}}>
               {'↑'}      
            </button>
            <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
               {'↓'}
            </button>
         </span>
      )
   }

   renderSortingButtonsCategory(asce_f, desc_f, text1, text2, t){
      return(
         <span style={{fontSize: "20px"}}>
            <button type="button" class="btn btn-primary btn-sm" onClick={()=> {asce_f.call(t)}}>
               {text1}
            </button>
            <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
               {text2}
            </button>
         </span>
      )
   }

   renderSingleFuncDropdown(title, items, func, t){
      return(
         <DropdownButton as={ButtonGroup} drop='down' variant='info' size='lg' id="dropdown-basic-button" title={title}>
            <div style={{maxHeight: '300px', overflow: 'scroll'}}>
               {items.map((item_title, index) => {
                  return(
                     <Dropdown.Item onClick={() => {func.call(t, item_title)}}>
                        {item_title}
                     </Dropdown.Item>
                  );
               
               })}
            </div>
         </DropdownButton>
      )
   }
   // does not need to be scrollable for in this case
   renderDropdown(title, items, funcs, t){
      return(
         <DropdownButton as={ButtonGroup} drop='down' variant='info' size='lg' id="dropdown-basic-button" title={title}>
               {items.map((item_title, index) => {
                  return(
                     <Dropdown.Item onClick={() => {funcs[index].call(t)}}>
                        {item_title}
                     </Dropdown.Item>
                  );
               
               })}
         </DropdownButton>
      )
   }

   // pagination methods ---------
   async firstPage() {
      const offset2 = 1
      this.changePage(offset2)
   }

   async prevEll(){
      const offset2 = Math.max(this.state.offset - 3, 1)
      this.changePage(offset2)
   }

   async previousPage() {
      const offset2 = this.state.offset - 1
      this.changePage(offset2)
   }

   async nextPage() {
      const offset2 = this.state.offset + 1
      this.changePage(offset2)
   }

   async nextEll(){
      const offset2 = Math.min(this.state.size, this.state.offset + 3)
      this.changePage(offset2)
   }

   async lastPage() {
      const offset2 = this.state.size
      this.changePage(offset2)
   }

   async selectPage(index){
      const offset2 = index
      this.changePage(offset2)
   }


   renderButtons() {

      let rows = []
      rows.push(
         <ModelPagination  
            size={this.state.size}
            current={this.state.offset}
            selectPage={this.selectPage}
            prev={this.previousPage}
            next={this.nextPage}
            first={this.firstPage}
            last={this.lastPage}
            nextEll={this.nextEll}
            prevEll={this.prevEll}
         />
      )
      

      
      const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

      rows.push(
         <React.Fragment>
            <tr style={{fontSize: '22px'}}>
               <th scope="col" class="text-center" >
                  {this.renderSingleFuncDropdown("First Name", letters, this.firstNameFilter, this)}
                  <br/>
                  {this.renderSortingButtonsUpDown(this.firstNameSortAsce, this.firstNameSortDesc, this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("Last Name", letters, this.lastNameFilter, this)} 
                  <br/>
                  {this.renderSortingButtonsUpDown(this.lastNameSortAsce, this.lastNameSortDesc, this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderDropdown("Party", ["Democrats", "Republicans"], [this.democratFilter, this.republicanFilter], this)}
                  <br/>
                  {this.renderSortingButtonsCategory(this.democratSort, this.republicanSort, 'D', 'R', this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderSingleFuncDropdown("State", this.state.state_list, this.stateFilter, this)}
                  <br/>
                  {this.renderSortingButtonsCategory(this.stateSortAsce, this.stateSortDesc, 'A-Z', 'Z-A', this)}
               </th>
               <th scope="col" class="text-center">
                  {this.renderDropdown("Office", ["House", "Senate"], [this.houseFilter, this.senateFilter], this)} 
                  <br/>
                  {this.renderSortingButtonsCategory(this.houseSort, this.senateSort, 'H', 'S', this)}
               </th>
            </tr>

         </React.Fragment>
      )

      return(
         <thead>
            {rows}
            
         </thead>
      )
   }

   handleSubmit(event) {
      //this.setState({formVal: event.target.value});
      if (this.state.formVal == "") {
         this.state.search = ""
         this.updateParameters()
      }
      else {
         this.setState({loaded:false})
         this.search()
         this.setState({loaded:true})
      }

      event.preventDefault();
   }


   async search() {
      this.state.search = "&search=" + this.state.formVal.replace(/\s+/g, '')
      this.updateParameters()
   }

   async changePage(offset) {
      this.setState({loaded: false, offset : offset})
      this.state.url = url+offset+this.state.parameters
      const response = await fetch(this.state.url)
      const data = await response.json();
      this.setState({loaded: true, Candidates: data.data, size: Math.ceil(data.size/perPage)})
   }

   async updateParameters() {
      const offset = 1
      this.state.parameters = this.state.first_name+this.state.last_name+this.state.party+this.state.office+this.state.search+this.state.sort_column+this.state.asc_or_desc+this.state.State
      this.state.url = url+offset+this.state.parameters

      this.setState({loaded : false})
         const response = await fetch(this.state.url)
         const data = await response.json();
         this.setState({loaded: true, Candidates: data.data, size: Math.ceil(data.size/perPage), offset: 1})

   }

   handleChange(event){
      this.state.formVal = event.target.value
      //this.setState({formVal:event.target.value})
      if (this.state.formVal == "") {
         this.state.search = ""
         this.updateParameters()
      }
      else {
         this.search()
      }
     
      this.setState({loaded: true})
   }

   resetFilters() {
      let newstate = { 
         loaded: false,
         Candidates: [],
         size: -1,
         offset: 1,
         first_name: "",
         last_name: "",
         party: "",
         office: "",
         search: "",
         State:  "",
         sort_column: "",
         asc_or_desc: "",
         formVal: "",
         parameters: "",
         url : ""
      }
      this.state = newstate
      this.setState(newstate)
      this.updateParameters()

   }

   render() {

      if (!this.state.loaded) {
          return (<div>Loading...</div>)
       }

      
    return (
         <div>
            
            <button type="button" class="btn btn-primary btn-sm" onClick={()=> {this.resetFilters.call()}}>
               {'Reset Filters'}      
            </button>
            <h1 id='title'>Candidates</h1>
            <h4>Page: {this.state.offset}/{this.state.size}</h4>
            <form onSubmit={this.handleSubmit}>
               <label>
                  <input type="text" value={this.state.formVal} onChange={this.handleChange} />
               </label>
               <input type="submit" value="Search" />
            </form>
            <table class='table'>   
               {this.renderButtons()}
            </table>      
            <Row>   
               {this.renderTableData()}
            </Row>
         </div>
     )
   }

}