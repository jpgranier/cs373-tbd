import React, { Component } from 'react';
import {Row, Col, Tab, Tabs} from 'react-bootstrap';
import CandidateTable from './CandidateTable';
import VotePieChart from './VotePieCarousel';


class CandidateActionList extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading : null,
            candTable : {},
            stageList : {},  //maps actionid to stage
            validActions : [],
            activeKey : null
        };
    }

    
    componentDidMount(){
        //we get a list of strings that are actionIds for this bill, so we need
        //to call api fetchs on all of them, but we can't move on until they have
        //all been completed for each step
        let api_calls = this.props.actionList.map((data) => 
            fetch("https://api.thefundsdontstop.me/getCandidateBillsByActionId?actionId=" + data));
        Promise.all(api_calls)
        .then(data => {
            return Promise.all(data.map(
                val => {
                    return val.json()
                    })
            )})
        .then(results => {
            results.forEach((candBillList, index) => {
                if(candBillList.length > 0){
                    this.state.stageList[candBillList[0]["actionId"]] 
                            = candBillList[0]["stage"]; // links actionId to stage for tabs          
                    this.state.candTable[candBillList[0]["actionId"]] = candBillList; //associates actionId with billList
                    this.state.validActions.push(candBillList[0]["actionId"]);
                }
            })

        })
        .then(finished =>{
            this.state.validActions.sort();
            this.setState({activeKey: this.state.validActions[0]});
            this.setState({isLoading : false});
        });

    }

    componentDidCatch(error, info){
        //react min pie chart throws an error b/c their library is a lil broken right now,
        //but everything visual still works so this exists just to catch that error and take
        //note of it
        console.log("react min pie chart throws error but it's all good : )")
    }

    whenSelected(selectedKey){
        this.setState({activeKey : selectedKey})
    }

    render(){
        if(this.state.isLoading === null){
            return(
                <h3>
                    ... Loading ...
                </h3>
            )
        }

     
        return (
            <Row className="p-1">
                <Col>
                        <Tabs activeKey={this.state.activeKey} onSelect={this.whenSelected.bind(this)}>
                            {this.state.validActions.map((data, index) => (
                                <Tab eventKey={data} key={data} title={this.state.stageList[data]} >
                                    <CandidateTable candidateList={this.state.candTable[data]} 
                                                    candidateNames={this.props.candidateNames} 
                                                    candidateParties={this.props.candidateParties}/>
                                                    
                                </Tab>
                            ))}
                        </Tabs>
                </Col>
                <Col sm={7}>
                    <h3>
                        <VotePieChart   key={this.state.activeKey} 
                                        candidateList={this.state.candTable[this.state.activeKey]} 
                                        candidateNames={this.props.candidateNames} 
                                        candidateParties={this.props.candidateParties}
                                        stage={this.state.stageList[this.state.activeKey]}/>
                          
                    </h3>
                </Col>
            </Row>
        )
        

    }
}

export default CandidateActionList;