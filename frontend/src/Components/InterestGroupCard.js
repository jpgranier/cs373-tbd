import React from 'react';
import {Container, Table} from 'react-bootstrap'

class InterestGroupCard extends React.Component {
  render() {
    const { interest } = this.props;
    let {
      name, description, address, phone, dems, repubs, gave_to_pac, gave_to_cand, total
    } = interest;

    if (dems == -1)
      dems = 'N/A'
    if (repubs == -1)
      repubs = 'N/A'
    if (gave_to_pac == -1)
      gave_to_pac = 'N/A'
    if (gave_to_cand == -1)
      gave_to_cand = 'N/A'
    if (total == -1)
      total = 'N/A'

    return (
      <Container className="p-3 border rounded-lg bg-light">
        <h3>{'Interest Group'}</h3>
        <Table>
          <tbody>
            <tr>
              <th>Description:</th>
                <td>{description}</td>
            </tr>
            <tr>
              <th>Mailing Address:</th>
              <td>
                { address }
              </td>
            </tr>
            <tr>
              <th>Phone:</th>
              <td>{ phone }</td>
            </tr>
            <tr>
              <th>Total Contributions:</th>
              <td>${ total }</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default InterestGroupCard;
