import React, {Component} from 'react'
import bb from 'billboard.js'

class LG_vis extends Component {

    state = {
        info : null,
        barchart : null
    }
    componentDidMount() {
		fetch("https://api.thefundsdontstop.me/getAllVotesmartBills")
		.then((res) => res.json())
		.then(data =>{
            this.setState({info : data})           			
        })
        .then(finished => {
            var _2014 = 0;
            var _2015= 0;
            var _2016= 0;
            var _2017= 0;
            var _2018= 0;
            var _2019= 0;
            var _2020= 0;
            var c;
            for (c = 0; c < this.state.info.length; c++) {
                console.log (this.state.info[0]);
                if (this.state.info[c].date_introduced === "2014") {
                    _2014++;
                }
                else if (this.state.info[c].date_introduced === "2015") {
                    _2015++;
                }
                else if (this.state.info[c].date_introduced === "2016") {
                    _2016++;
                }
                else if (this.state.info[c].date_introduced === "2017") {
                    _2017++;
                }
                else if (this.state.info[c].date_introduced === "2018") {
                    _2018++;
                }
                else if (this.state.info[c].date_introduced === "2019") {
                    _2019++;
                }
                else if (this.state.info[c].date_introduced === "2020") {
                    _2020++;
                }
            }
            var chart = bb.generate({
                data: {
                  columns: [
                  ["2014", _2014],
                  ["2015", _2015],
                  ["2016", _2016],
                  ["2017", _2017],
                  ["2018", _2018],
                  ["2019", _2019],
                  ["2020", _2020],
                  ],
                  type: "bar"
                },
                axis: {
                    x: {
                        label: "Year"
                    },
                    y: {
                        label: "Number of bills introduced"
                    }
                },
                bar: {
                  width: {
                    ratio: 0.5
                  }
                },
                
              });
            this.state.barchart = chart						
        })
    }
    
	render() {
		return (
			<div>
                {this.state.barchart}
            </div>
        )
	}
    
}

export default LG_vis;