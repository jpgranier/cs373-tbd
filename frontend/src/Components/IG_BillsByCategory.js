import React from 'react';
import {Nav, Container, Col, Row, Table} from 'react-bootstrap'

class IG_BillsByCategory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        bills: null,
        isLoaded : null
      }
  }

  componentDidMount() {
    const { category } = this.props;

    fetch('https://api.thefundsdontstop.me/getBillsByCategoryId?category_id=' + category)
    .then(res => res.json())
    .then(data => {
        this.setState({bills: data.bills}) //fill
        this.setState({isLoaded : true})
    })
    .catch(console.log)
  }

  displayBills = () => {

    if (this.state.isLoaded === null){
      return (<div>Error fetching data. Please refresh the page.</div>)
    }

    let rows = [];

    rows = this.state.bills.map((bill, ) => {
    return(
        <React.Fragment>
        <tr>
          <td>{<Nav.Link href={"/bills/" + bill.bill_id }>
          <p>{bill.title}</p></Nav.Link>}</td>
          <td>{bill.bill_number}</td>
          <td>{bill.date_introduced}</td>
        </tr>

        </React.Fragment>
    )
    })


    return rows;
  };


  render() {
    return (
        <React.Fragment>
          <div class="scrollable_table sticky-header">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Number</th>
                        <th>Date Introduced</th>
                    </tr>
                </thead>
          <tbody>
            {this.displayBills()}
          </tbody>
        </Table>
        </div>
        </React.Fragment>
    );
  }
}

export default IG_BillsByCategory;
