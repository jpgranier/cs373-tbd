import React, {Component} from 'react'
import {Nav, Table} from 'react-bootstrap';
import '../Styles/CandidateInstance.css'

class Candidate_BillDecisions extends Component {
    state = {
        votes : null,
        isLoaded : null
    }

    createTable = () => {
        
        var len = this.state.votes.length       
        let rows = new Array(len)
        var i;
        for (i = 0; i < len; i++) {
            rows.push(<tr><td><Nav.Link href={"/bills/" + this.state.votes[i].bill_id}>{this.state.votes[i].title}</Nav.Link></td><td>{this.state.votes[i].vote}</td></tr>)
        }
        return rows
    }

    componentDidMount() {
        fetch("https://api.thefundsdontstop.me/getVotesByCandidate?votesmart_id=" + this.props.votesmart_id)
        .then((res) => res.json())
		.then(data =>{
            this.setState({votes : data.votes})
            return data.votes
        })
        .then(data =>{
            this.setState ({isLoaded : true})
        })
    }

    render() {
        if (this.state.isLoaded === null) {
			return (
				<div>
					<b> Loading </b>
				</div>
			)
        }
        return (

            <div class="scrollable_table sticky-header">
            <Table striped bordered hover>
            <thead>
                <tr>
                <th>Bill Name</th>
                <th>Vote</th>
                </tr>
            </thead>
            <tbody>
                {this.createTable()}
            </tbody>
            </Table>

            </div>


        );


    }
}

export default Candidate_BillDecisions;