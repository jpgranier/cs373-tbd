import React, {Component} from 'react'
import {Table} from 'react-bootstrap'

class Candidate_FinancialContributions extends Component {	
	
	state = {
		isLoaded : null,
        info : null,
        indNames : null,
        indTotals : null
	}
	
	componentDidMount() {
	    fetch("https://api.thefundsdontstop.me/getContactInfoByVoteSmartId?votesmart_id=" + this.props.votesmart_id)
            .then(res => res.json())
            .then(data => {return data['crp_id']})
            .then(data => {return fetch("https://api.thefundsdontstop.me/getTopIndustriesByCrpId?crp_id=" + data)})
		.then((res) => res.json())
		.then(data =>{
			this.setState({info : data})			
		})
		.then(finished => {
            var ind1Data = this.state.info.ind1.split("Total: ")
            var ind2Data = this.state.info.ind2.split("Total: ")
            var ind3Data = this.state.info.ind3.split("Total: ")
            var ind4Data = this.state.info.ind4.split("Total: ")
            var ind5Data = this.state.info.ind5.split("Total: ")
            var indNames = new Array(5)
            var indTotals = new Array(5)
            indNames[0] = ind1Data[0]
            indTotals[0]= "$" + ind1Data[1]
            
            indNames[1] = ind2Data[0]
            indTotals[1] = "$" + ind2Data[1]
            
            indNames[2] = ind3Data[0]
            indTotals[2] = "$" + ind3Data[1]
            
            indNames[3] = ind4Data[0]
            indTotals[3]= "$" + ind4Data[1]
            
            indNames[4] = ind5Data[0]
            indTotals[4]= "$" + ind5Data[1]
            this.setState({indNames : indNames})
            this.setState({indTotals : indTotals})
			this.setState({isLoaded : true})			
		})
	}
	render() {
		if (this.state.isLoaded === null) {
			return (
				<div>
					<b> Loading </b>
				</div>
			)
        }
        
        let rows = []

        for (let a = 0; a < 5 ; a++) {
            rows.push(
                <tr>
                <td>{this.state.indNames[a]}</td>
                <td>{this.state.indTotals[a]}</td>           
                </tr>
            )
        }
		return (
      <div>
	  <Table striped bordered hover>
        <thead>
            <tr>
            <th>Industry</th>
            <th>Contribution</th>
            </tr>
        </thead>
        <tbody>
            {rows}
        </tbody>
        </Table>
	  </div>
			 );
	}
}

export default Candidate_FinancialContributions;