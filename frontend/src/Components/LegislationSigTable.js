import React, { Component } from 'react';
import {Nav, Table} from 'react-bootstrap';

// takes in siglist into properties, should be list of sig

class LegislationSigTable extends Component{
    render() {
        return (
            <Table striped bordered hover>
                <tbody>
                    {
                        this.props.sigList.map((sig) => (
                            <tr key={sig.sig_id}>
                                <td>
                                    <Nav.Link href={"/interest_groups/" + sig.votesmart_id}>
                                        {sig.name}
                                    </Nav.Link>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        );
    }
}

export default LegislationSigTable;