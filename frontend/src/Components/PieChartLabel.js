import React, { Component } from 'react';
import { OverlayTrigger, Button, Popover } from 'react-bootstrap';
import '../Styles/PieButton.css'
class PieChartLabel extends Component{
    constructor(props){
        super(props);
        this.state = {
            //map data to titles and css files
            title_map : {
                "Republican-No" : "Rep-No",
                "Republican-Yes" : "Rep-Yes",
                "Democrat-No" : "Dem-No",
                "Democrat-Yes" : "Dem-Yes",
                "Democrat-Sponsor" : "Dem-Spon",
                "Democrat-CoSponsor" : "Dem-Co",
                "Republican-Sponsor" : "Rep-Spon",
                "Republican-CoSponsor" : "Rep-Co"
            },
            css_map : {
                "Republican-No" : "republican_no",
                "Republican-Yes" : "republican_yes",
                "Democrat-No" : "democratic_no",
                "Democrat-Yes" : "democratic_yes",
                "Democrat-Sponsor" : "democratic_yes",
                "Democrat-CoSponsor" : "democratic_no",
                "Republican-Sponsor" : "republican_yes",
                "Republican-CoSponsor" : "republican_no"
            }
        }
    }

    render(){
        
  
        return(
            
            
            <foreignObject   
                x={45 + this.props.dx} y={17 + this.props.dy}
                    width="15" height="50"
                >
                    <div xmlns="http://www.w3.org/1999/xhtml">
                        <OverlayTrigger trigger="hover" overlay={
                            <Popover id="pie_info_popover">
                                <Popover.Title as="h1"></Popover.Title>
                                    <Popover.Content >
                                        {this.props.data[this.props.dataIndex].title}
                                        <br></br>
                                        {this.props.data[this.props.dataIndex].value} votes
                                        <br></br>
                                        {this.props.data[this.props.dataIndex].percentage.toFixed(2)}%
                                    </Popover.Content>
                                </Popover>}>
                            <Button bsPrefix={this.state.css_map[this.props.data[this.props.dataIndex].title]}>
                                    {this.state.title_map[this.props.data[this.props.dataIndex].title]}
                            </Button>
                        </OverlayTrigger>
                    </div>
                </foreignObject>
                    
            
        )
        
        
    }

}

export default PieChartLabel;