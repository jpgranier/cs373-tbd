import React, { Component } from 'react';
//import {Nav, NavBar} from 'react-bootstrap';
//import styled from 'styled-components';


import {
    Button,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
  } from 'reactstrap';

//const Styles = styled.div

export class NavigationBar extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }

      render(){
          return(
            <Navbar color="light" light expand="md" sticky="top">
                <NavbarBrand href="/">The Funds Don't Stop</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar/>
                <Nav className="ml-auto" navbar>
                    <NavItem><NavLink href="/">Home</NavLink></NavItem>
                    <NavItem><NavLink href="/interests">Interest Groups</NavLink></NavItem>
                    <NavItem><NavLink href="/candidates">Candidates</NavLink></NavItem>
                    <NavItem><NavLink href="/legislation">Legislation</NavLink></NavItem>
                    <NavItem><NavLink href="/visualizations">Visualizations</NavLink></NavItem>
                    <NavItem><NavLink href="/about">About</NavLink></NavItem>
                </Nav>
            </Navbar>
          )
      }
}


