import React, { Component } from 'react';
import VotePieChart from './VotePieChart';
import { Carousel } from 'react-bootstrap';

class VotePieCarousel extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded : null,
            data : null,
            classes : [1, 2, 3]
        };
    }
    componentDidMount(){
        //here we can do all of the processing for the Pie charts and just pass it in
        if(this.props.stage === "Introduced"){
            var DemocraticSponsor = {   color: '#4B8DFA',
                                    title: 'Democrat-Sponsor',
                                    value: 0};
            var RepublicanSponsor = {   color: '#E4444B',
                                    title: 'Republican-Sponsor',
                                    value: 0};
            this.props.candidateList.forEach((candidate) => {
                if(candidate.vote === 'S'){
                    if(this.props.candidateParties[candidate.candidateId] === 'R'){
                        RepublicanSponsor.value++;
                    }
                    else{
                        DemocraticSponsor.value++;
                    }
                }
            });
            var DemocraticCo = {   color: '#084174',
                                    title: 'Democrat-CoSponsor',
                                    value: 0};
            var RepublicanCo = {   color: '#B04039',
                                    title: 'Republican-CoSponsor',
                                    value: 0};

            this.props.candidateList.forEach((candidate) => {
                if(candidate.vote === 'C'){
                    if(this.props.candidateParties[candidate.candidateId] === 'R'){
                        RepublicanCo.value++;
                    }
                    else{
                        DemocraticCo.value++;
                    }
                }
            });
            this.setState({data: [DemocraticSponsor, RepublicanSponsor, DemocraticCo, RepublicanCo]});
            this.setState({isLoaded: true});
        }
        else{
            var DemocraticYes = {   color: '#4B8DFA',
                                    title: 'Democrat-Yes',
                                    value: 0};
            var RepublicanYes = {   color: '#E4444B',
                                    title: 'Republican-Yes',
                                    value: 0};
            this.props.candidateList.forEach((candidate) => {
                if(candidate.vote === 'Y'){
                    if(this.props.candidateParties[candidate.candidateId] === 'R'){
                        RepublicanYes.value++;
                    }
                    else{
                        DemocraticYes.value++;
                    }
                }
            });
            var DemocraticNo = {   color: '#084174',
                                    title: 'Democrat-No',
                                    value: 0};
            var RepublicanNo = {   color: '#B04039',
                                    title: 'Republican-No',
                                    value: 0};
            this.props.candidateList.forEach((candidate) => {
                if(candidate.vote === 'N'){
                    if(this.props.candidateParties[candidate.candidateId] === 'R'){
                        RepublicanNo.value++;
                    }
                    else{
                        DemocraticNo.value++;
                    }
                }
            });
            this.setState({data: [ DemocraticNo, DemocraticYes, RepublicanYes, RepublicanNo]});
            this.setState({isLoaded: true});
        }
    }

    render(){
        if(this.state.isLoaded === null){
            return(
                <div>
                    ... Loading ...
                </div>
            )
        }
        return(
            <Carousel style={{backgroundColor : '#101010', height: '600px'}}>
                {
                    this.state.classes.map((pie_class) => (
                        <Carousel.Item key={pie_class}>
                            <VotePieChart   pie_class={pie_class} 
                                            data={this.state.data} 
                                            stage={this.props.stage}/>
                            <Carousel.Caption>
                                <p style={{ fontFamily: 'sans-serif',
                                            fontSize: '15px',
                                            paddingBottom: '33px'}}>
                                                Hover over bubbles for more info
                                                </p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        
                    ))
                }
            </Carousel>
        )
    }
}
export default VotePieCarousel;