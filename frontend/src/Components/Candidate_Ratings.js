import React, {Component} from 'react'
import {Nav, Table} from 'react-bootstrap'
import '../Styles/CandidateInstance.css'

class Candidate_Ratings extends Component {	
	
	state = {
        isLoaded : null,
        info : null,
        ratings : null, //ratings of recent 5
        sig_ids : null,
        ig_names: null //interest group names
	}
	
	componentDidMount() {      
		fetch("https://api.thefundsdontstop.me/getRatingsByCandidate?votesmart_id=" + this.props.votesmart_id)
		.then((res) => res.json())
		.then(data =>{
          this.setState({info : data})
          var a;
          var len = this.state.info.length
          this.state.ratings = new Array(len);
          this.state.sig_ids = new Array(len);  
          for (a = 0; a < len ; a++) {
              this.state.ratings[a] = this.state.info[a].rating
              this.state.sig_ids[a] = this.state.info[a].sig_id
          }
        })
        .then(data => {
            return fetch("https://api.thefundsdontstop.me/getInterestGroupIds")
        })
        .then((result) => result.json())
        .then(data => {
            var iglist = data
            var result = {}
            iglist.forEach(igdict => {
                result[igdict.votesmart_id] = igdict.sig_name
            })
            var len = this.state.ratings.length
            this.state.ig_names = new Array(len)
            var k;
            for (k = 0; k < len; k++) {
                this.state.ig_names[k] = result[this.state.sig_ids[k]]
            }
            
            this.setState({isLoaded : true})
        })
        
	}
	render() {
		if (this.state.isLoaded === null) {
			return (
				<div>
					<b> Loading </b>
				</div>
			)
    }
    let rows = []
    let len = this.state.info.length

    for (let a = 0; a < len ; a++) {
      rows.push(
        <React.Fragment>
          <tr>
              <td>
              <Nav.Link href={"/interest_groups/" + this.state.sig_ids[a]}>{this.state.ig_names[a]}</Nav.Link>
              </td>
              <td>
              {this.state.ratings[a]}
              </td>
          </tr>

        </React.Fragment>
      )
  }
		return (
      
          <div class="scrollable_table sticky-header">
            <Table striped bordered hover>
              <thead>
                  <tr>
                  <th>Interest Group</th>
                  <th>Rating (%)</th>
                  </tr>
              </thead>
              <tbody>                
                {rows}
              </tbody>
              </Table>
            </div>
			 );
	}
}

export default Candidate_Ratings;