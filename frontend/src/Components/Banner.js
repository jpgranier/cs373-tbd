import React from 'react';
import money from '../Assets/money.jpg'; //https://upload.wikimedia.org/wikipedia/commons/7/7b/Obverse_of_the_series_2009_%24100_Federal_Reserve_Note.jpg
import styled from 'styled-components';
import {Jumbotron, Container} from 'react-bootstrap';

const Styles = styled.div`
  .banner {

    background: url(${money}) no-repeat fixed top; 
    height: 800px;
    background-size: cover;
    opacity: .8;
    
    position: relative;
    left:0;
    right:0;
    bottom:0px;
    z-index:-200;
  }`;

export const Banner = () => (
    <Styles>
        <Jumbotron fluid className="banner">
            <Container>
            <h2>
                <font color={99999}>
            
                </font>
            </h2>
            </Container>
        </Jumbotron>
    </Styles>
)