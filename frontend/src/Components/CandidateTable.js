import React, { Component } from 'react';
import {Table, Nav} from 'react-bootstrap';

// passed in candidateActionList which is a list of candidateActions (/getCandidateBillsByActionIds)
class CandidateTable extends Component{

    constructor(props) {
        super(props);
        this.state = {
            formVal: "",
            isLoaded : null
          }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async search() {
        this.setState({isLoaded: true});
    }
      
    handleChange(event){
        this.state.formVal = event.target.value
        this.search()
    }
      
    handleSubmit(event) {
        this.setState({isLoaded:false})
        this.search()
        this.setState({isLoaded:true})
        event.preventDefault();
    }

    displayCandidateList = () => {
        let rows = [];
        rows = this.props.candidateList.map((candidate) => {
        let cand_name = this.props.candidateNames[candidate.candidateId]
        if(cand_name == null)
            cand_name = '<UNLISTED>'
        if(cand_name.toLowerCase().search(this.state.formVal.toLowerCase()) == -1)
            return(null)
        return(
            <React.Fragment>
                <tr key={candidate.candidateId}>
                    <td>
                        <Nav.Link href={"/candidate/" + candidate.candidateId} >
                            {this.props.candidateNames[candidate.candidateId]}
                        </Nav.Link>
                        
                    </td>
                    <td>
                        {candidate.office}
                    </td>
                    <td> 
                        {this.props.candidateParties[candidate.candidateId]}
                    </td>
                    <td>
                        {candidate.vote}
                    </td>
                </tr>
            </React.Fragment>
        )})
        return rows;
    }
    render() {
        
        return (
            <React.Fragment>
            <form onSubmit={this.handleSubmit}>
            <label>
                <input type="text" value={this.state.formVal} onChange={this.handleChange} />
            </label>
                <input type="submit" value="Search Name" />
            </form>
            <div className="scrollable_table sticky-header">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Office </th>
                            <th> Party </th>
                            <th> Vote </th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.displayCandidateList()
                        }
                    </tbody>
                </Table>
            </div>
            </React.Fragment>


        );
    }
}

export default CandidateTable;