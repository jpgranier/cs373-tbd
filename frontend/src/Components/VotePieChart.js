import React, { Component } from 'react';
import ReactMinimalPieChart from 'react-minimal-pie-chart';
import PieChartLabel from './PieChartLabel';

class VotePieChart extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : [],
            isLoaded : null,
            titles: {2 : 'Party Distrubition Voting Yes',
                     3 : 'Party Distrubition Voting No',
                     1 : 'Overall Votes'
                    }
        };
    }
    
        

    componentDidMount(){
        var prunedData = null
        console.log(this.props.data)
        // different set of titles if the stage is a bill introduction
        if(this.props.stage === 'Introduced'){
            this.setState({titles: {2 : 'Sponsor Party Distrubition',
                                    3 : 'Co-Sponsor Party Distrubition',
                                    1 : 'Overall Sponsors/Co-Sponsors'
           }})
        }
        if(this.props.pie_class === 1){
            prunedData = [...this.props.data]
        }
        else if(this.props.pie_class === 2){
            prunedData =  [this.props.data[1], this.props.data[2]]
        }
        else{
            prunedData = [this.props.data[0], this.props.data[3]]
        }
        for(let i = prunedData.length - 1; i >= 0; --i){
            if(prunedData[i].value === 0){
                prunedData.splice(i, 1)
            }
        }
        this.setState({data : prunedData})
        this.setState({isLoaded: true});
    }

    render(){
        if(this.state.isLoaded === null){
            return(
                <h2>
                    ... Loading ...
                </h2>
            )
        }
        return(
            <div>
                <h1 style={{color: '#EEEEEE', 
                            fontFamily: 'sans-serif', 
                            textAlign: 'center', 
                            paddingTop: '15px'}}>
                    {this.state.titles[this.props.pie_class]}
                </h1>
                <ReactMinimalPieChart
                                animate
                                animationDuration={500}
                                animationEasing="ease-out"
                                cx={50}
                                cy={43}
                                data={this.state.data}
                                label={false}
                                labelPosition={45}
                                lengthAngle={-360}
                                lineWidth={100}
                                paddingAngle={0}
                                radius={25}
                                rounded={false}
                                startAngle={0}
                                label={<PieChartLabel/>}
                                labelPosition={140}

                                viewBoxSize={[
                                    100,
                                    100
                                ]}/>
            </div>
                            
                         
        )
    }
}
export default VotePieChart;