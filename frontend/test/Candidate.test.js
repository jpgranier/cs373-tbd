import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect, should} from 'chai';


import CandidatePage from '../src/Pages/CandidatePage';
import Candidate_Info from '../src/Components/Candidate_Info'
import Candidate_FinancialContributions from '../src/Components/Candidate_FinancialContributions'
import Candidate_Ratings from '../src/Components/Candidate_Ratings'
import Candidate_BillDecisions from '../src/Components/Candidate_BillDecisions'

var assert = require('assert')


configure({ adapter: new Adapter() });

describe('CandidatePage', () => {
  const wrapper = shallow(
    <CandidatePage match={{params: {votesmart_id: 15691}}}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    expect(wrapper.find('Candidate_Info').length).to.equal(1)
    expect(wrapper.find('Candidate_FinancialContributions').length).to.equal(1)
    expect(wrapper.find('Candidate_Ratings').length).to.equal(1)
    expect(wrapper.find('Candidate_BillDecisions').length).to.equal(1)

  });

});

describe('Candidate_Info', () => {
  const wrapper = shallow(
    <Candidate_Info  votesmart_id={15691} />
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    setTimeout(() => {
    wrapper.update();
    expect(wrapper.state().isLoaded).is.true;
    expect(wrapper.find('Tw_display').length).to.equal(1)
    }, 2000);
  });

  it('API returns', async () => {
    await fetch('https://api.thefundsdontstop.me/getContactInfoByVoteSmartId?votesmart_id=15691')
        .then((res) => {
            return res.json()
        })
        .then((res) => {
            assert.equal(res.id, 1)
      })
    })
});


describe('Candidate_FinancialContributions', () => {

    const wrapper = shallow(
        <Candidate_FinancialContributions votesmart_id={15691} />
      );
    
      it('is defined', () => {
        expect(wrapper).to.not.be.undefined;
        expect(wrapper.state()).to.not.be.undefined;
      });
    
      it('is loaded', () => {
        setTimeout(() => {
        wrapper.update();
        expect(wrapper.state().isLoaded).is.true;
        expect(wrapper.state().info).is.not.null;
        expect(wrapper.state().ind1Name).is.not.null;
        }, 2000);
      });

});

describe('Candidate_Ratings', () => {

    const wrapper = shallow(
        <Candidate_Ratings votesmart_id={15691} />
      );
    
      it('is defined', () => {
        expect(wrapper).to.not.be.undefined;
        expect(wrapper.state()).to.not.be.undefined;
      });
    
      it('is loaded', () => {
        setTimeout(() => {
        wrapper.update();
        expect(wrapper.state().isLoaded).is.true;
        expect(wrapper.state().info).is.not.null;
        expect(wrapper.state().ig_names).is.not.null;
        }, 2000);
      });
  
  });

  describe('Candidate_BillDecisions', () => {
    const wrapper = shallow(
        <Candidate_BillDecisions votesmart_id={15691} />
      );
    
      it('is defined', () => {
        expect(wrapper).to.not.be.undefined;
        expect(wrapper.state()).to.not.be.undefined;
      });
    
      it('is loaded', () => {
        setTimeout(() => {
        wrapper.update();
        expect(wrapper.state().isLoaded).is.true;
        expect(wrapper.state().votes).is.not.null;
        }, 2000);
      });
  
  });