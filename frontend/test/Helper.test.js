import React from 'react'
import ReactDOM from 'react-dom';
import { expect } from 'chai';
import { mount, render, shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
//import Foo, { doIncrement, doDecrement, Counter } from '../src/Foo.js';

//import App from '../src/App'




configure({ adapter: new Adapter() });
global.expect = expect;
global.mount = mount;
global.render = render;
global.shallow = shallow;



// describe('Local State',() => {
//     it('should increment the counter in state', () => {
//       const state = { counter: 0 };
//       const newState = doIncrement(state);
//       expect(newState.counter).to.equal(1);
//     });
//     it('should decrement the counter in state', () => {
//       const state = { counter: 0 };
//       const newState = doDecrement(state);
//       expect(newState.counter).to.equal(-1);
//     });
//   });

//   describe('App Component', () => {
//     it('renders the Counter wrapper', () => {
//       const wrapper = shallow(<Foo />);
//       expect(wrapper.find(Counter)).to.have.length(1);
//     });

//     it('passes all props to Counter wrapper', () => {
//       const wrapper = shallow(<Foo />);
//       let counterWrapper = wrapper.find(Counter);
//       expect(counterWrapper.props().counter).to.equal(0);
//       wrapper.setState({ counter: -1 });
//       counterWrapper = wrapper.find(Counter);
//       expect(counterWrapper.props().counter).to.equal(-1);
//     });

//     it('increments the counter', () => {
//       const wrapper = shallow(<Foo />);
//       wrapper.setState({ counter: 0 });
//       wrapper.find('button').at(0).simulate('click');
//       expect(wrapper.state().counter).to.equal(1);
//     });

//     it('decrements the counter', () => {
//       const wrapper = shallow(<Foo />);
//       wrapper.setState({ counter: 0 });
//       wrapper.find('button').at(1).simulate('click');
//       expect(wrapper.state().counter).to.equal(-1);
//     });


//   });






