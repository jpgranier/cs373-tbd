import React from 'react'
import ReactDOM from 'react-dom';
import { expect } from 'chai';
import { mount, render, shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LegislationMount from '../src/Pages/LegislationMount.js';
import LegislationInstance from '../src/Pages/LegislationInstance.js';
import LegislationSigTable from '../src/Components/LegislationSigTable.js';
import LegislationSponsorTable from '../src/Components/LegislationSponsorTable.js';
import CandidateActionList from '../src/Components/CandidateActionList.js';
import VotePieChart from '../src/Components/VotePieChart.js'
import { Row, Container, Table, Tabs, Col} from 'react-bootstrap';
import CandidateTable from '../src/Components/CandidateTable.js';

// code taken from https://github.com/enzymejs/enzyme/issues/942 to set up jsdom
var jsdom = require('jsdom');
var assert = require('assert')
const { JSDOM } = jsdom;
const { document } = (new JSDOM('')).window;



configure({ adapter: new Adapter() });
global.expect = expect;
global.mount = mount;
global.render = render;
global.shallow = shallow;
global.document = document;
global.window = document.defaultView['window']



describe('LegislationMount', function() {
    this.timeout(5000)
    var component = null
    var load_promise = null
    before(() => {
        load_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 2500); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<LegislationMount match={{params: {bill_id: 27527}}}/>);
    });
    
    it('should not be undefined given bill_id', () => {
        return load_promise.then(() => {expect(component.find(LegislationInstance).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });
});

describe('LegislationInstance', function() {
    this.timeout(5000)
    var component = null
    var load_promise = null
    var legislation_instance = null
    before(() => {
        let load_mount_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 2500); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<LegislationMount match={{params: {bill_id: 27527}}}/>);
        load_promise = load_mount_promise
        .then(() => {
            legislation_instance = component.find(LegislationInstance).dive()
        });
    });
    
    it('should render Rows', () => {
        return load_promise.then(() => {expect(legislation_instance.find(Row).length).to.equal(4)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render 4 Rows', () => {
        return load_promise.then(() => {expect(legislation_instance.find(Row).length).to.equal(4)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should have a Table', () => {
        return load_promise.then(() => {expect(legislation_instance.find(Table).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should have CandidateActionList', () => {
        return load_promise.then(() => {expect(legislation_instance.find(CandidateActionList).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render Candidate Sponsors', () => {
        return load_promise.then(() => {expect(legislation_instance.find('h3').at(1).text()).to.equal('Candidate Sponsors')})
        .catch((err) => {
            assert.fail(err);
        })
    });
    it('should have LegislationSigTable', () => {
        return load_promise.then(() => {expect(legislation_instance.find(LegislationSigTable).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });
});

describe('LegislationSponsorTable', function()  {
    this.timeout(5000)
    var component = null
    var load_promise = null
    var legislation_st = null
    before(() => {
        let load_mount_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 2500); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<LegislationMount match={{params: {bill_id: 27527}}}/>);
        load_promise = load_mount_promise
        .then(() => {
            return component.find(LegislationInstance).dive()
        })
        .then((c) => {
            legislation_st = c.find(LegislationSponsorTable).dive()  
        })
    });
    
    it('should render a Table', () => {
        return load_promise.then(() => {expect(legislation_st.find(Table).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    
});

describe('LegisislationSigTable', function()  {
    this.timeout(5000)
    var component = null
    var load_promise = null
    var legislation_sig = null
    before(() => {
        let load_mount_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 3000); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<LegislationMount match={{params: {bill_id: 27527}}}/>);
        load_promise = load_mount_promise
        .then(() => {
            return component.find(LegislationInstance).dive()
        })
        .then((c) => {
            legislation_sig = c.find(LegislationSigTable).dive()  
        })
    });
    
    it('should render a Table', () => {
        return load_promise.then(() => {expect(legislation_sig.find(Table).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should have at least one interest group', () => {
        return load_promise.then(() => {expect(legislation_sig.find('td').length).to.be.above(0)})
        .catch((err) => {
            assert.fail(err);
        })
    });
});

describe('CandidateActionList', function() {
    this.timeout(5000)
    var component = null
    var load_promise = null
    var action_list = null
    before(() => {
        let load_mount_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 2500); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<LegislationMount match={{params: {bill_id: 27527}}}/>);
        load_promise = load_mount_promise
        .then(() => {
            return component.find(LegislationInstance).dive()
        })
        .then((c) => {
            action_list = c.find(CandidateActionList).dive()  
        })
        .then(() => {
            return new Promise((resolve, reject) => {
                setTimeout(() => resolve(999), 2000);
            })
        });
    });
    
    it('should render a Row', () => {
        return load_promise.then(() => {expect(action_list.find(Row).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should have two Col', () => {
        return load_promise.then(() => {expect(action_list.find(Col).length).to.equal(2)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render at least one CandidateTable', () => {
        return load_promise.then(() => {expect(action_list.find(CandidateTable).length).to.be.above(0)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render Tabs', () => {
        return load_promise.then(() => {expect(action_list.find(Tabs).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    
});


