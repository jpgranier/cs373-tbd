Member1 Name: Jonathan Granier
Member1 UTEID: jpg2778
Member1 GitLab ID: jpgranier
Member1 Estimated Completion Time: 85h
Member1 Actual Completion Time: 86h

Member2 Name: Randy Donaldson
Member2 UTEID: rgd546
Member2 GitLab ID: randizzleDee
Member2 Estimated Completion Time: 65h
Member2 Actual Completion Time: 74h

Member3 Name: Mahir Karim
Member3 UTEID: mak4222
Member3 GitLab ID: mahirkarim
Member3 Estimated Completion Time: 62h
Member3 Actual Completion Time: 75h

Member4 Name: Timothy Gan
Member4 UTEID: thg358
Member4 GitLab ID: timgan
Member4 Estimated Completion Time: 80h
Member4 Actual Completion Time: 90h

Member5 Name: Rithvik Vellaturi
Member5 UTEID: rkv279
Member5 GitLab ID: rith1
Member5 Estimated Completion Time: 66h
Member5 Actual Completion Time: 87h

Member6 Name: Roger Zhong
Member6 UTEID: rz5292
Member6 GitLab ID: Lapithes
Member6 Estimated Completion Time: 80h
Member6 Actual Completion Time: 75h


Git SHA: 84d179cf78dc933240243ec1882eb1a5eebd468c
Project Leader: Mahir Karim
GitLab Pipeline: https://gitlab.com/jpgranier/cs373-TheFundsDontStop/pipelines
Website: https://thefundsdontstop.me