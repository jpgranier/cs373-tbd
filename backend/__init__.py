from flask import Flask
from backend.api.candidate_bills import candidate_bills_api
from backend.api.candidate_financial_contributions import (
    candidate_financial_contributions_api,
)
from backend.api.contact_info import contact_info_api
from backend.api.interest_groups import interest_groups_api
from backend.api.ratings import ratings_api
from backend.api.votesmart_bills import votesmart_bills_api
from backend.models.populate_database import populate_database
from backend.models.models import db
import os


# mysql://username:password@backend/db
SQL_ALCHEMY_MYSQL_DATABASE_BASE_URI = "mysql://{}:{}@{}/{}"

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = SQL_ALCHEMY_MYSQL_DATABASE_BASE_URI.format(
    os.environ["DB_USER"],
    os.environ["DB_PASSWORD"],
    os.environ["DB_HOST"],
    os.environ["DB_DATABASE"],
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.register_blueprint(candidate_bills_api)
app.register_blueprint(candidate_financial_contributions_api)
app.register_blueprint(contact_info_api)
app.register_blueprint(interest_groups_api)
app.register_blueprint(ratings_api)
app.register_blueprint(votesmart_bills_api)
app.register_blueprint(populate_database)
db.init_app(app)


@app.route("/health")
def index():
    return "Healthy flask application."


if __name__ == "__main__":
    # db.create_all()
    app.run(debug=True, host="0.0.0.0")
