from backend.models.models import CandidateBills
from flask_cors import cross_origin
from flask import request
from flask import jsonify
from flask import Blueprint
import json


candidate_bills_api = Blueprint(
    "candidate_bills", __name__, template_folder="templates"
)


@candidate_bills_api.route("/getCandidateBillsByActionId", methods=["GET"])
@cross_origin()
def get_candidate_bills_by_action_id():
    candidate_bills = CandidateBills.query.filter_by(
        actionId=request.args.get("actionId")
    ).all()
    response = jsonify([cb.as_dict() for cb in candidate_bills])
    return response, 200


@candidate_bills_api.route("/getVotesByCandidate")
@cross_origin()
def get_votes_by_candidate():
    candidate_bills = (
        CandidateBills.query.filter_by(candidateId=request.args.get("votesmart_id"))
        .limit(10)
        .all()
    )
    response_dict = {
        "votesmart_id": request.args.get("votesmart_id"),
        "votes": [v.as_dict() for v in candidate_bills],
    }
    response = json.dumps(response_dict)
    return response, 200
