from backend.models.models import CandidateFinancialContributions
from flask_cors import cross_origin
from flask import request
from flask import Blueprint
import json

candidate_financial_contributions_api = Blueprint(
    "candidate_financial_contributions", __name__, template_folder="templates"
)


@candidate_financial_contributions_api.route("/getTopIndustries")
@cross_origin()
def get_top_industries():
    industries = CandidateFinancialContributions.query.filter_by(
        id=request.args.get("id")
    ).first()
    response = json.dumps(industries.as_dict())
    return response, 200


@candidate_financial_contributions_api.route("/getTopIndustriesByCrpId")
@cross_origin()
def get_top_industries_by_crp_id():
    industries = CandidateFinancialContributions.query.filter_by(
        crp_id=request.args.get("crp_id")
    ).first()
    response = json.dumps(industries.as_dict())
    return response, 200
