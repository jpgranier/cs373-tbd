from backend.models.models import Ratings
from flask_cors import cross_origin
from flask import request
from flask import jsonify
from flask import Blueprint

ratings_api = Blueprint("ratings", __name__, template_folder="templates")


@ratings_api.route("/getRatingsByCandidate")
@cross_origin()
def get_ratings_by_candidate():
    ratings = Ratings.query.filter_by(
        candidate_votesmart_id=request.args.get("votesmart_id")
    ).all()
    response = jsonify([r.as_dict() for r in ratings])
    return response, 200


@ratings_api.route("/getRecentRatings", methods=["GET"])
@cross_origin()
def get_recent_ratings():
    ratings = Ratings.query.filter_by(sig_id=request.args.get("sig_id")).limit(20).all()
    response = jsonify([r.as_dict() for r in ratings])
    return response, 200
