from flask import Blueprint
from backend.models.models import db
from backend.models.models import CandidateBills
from backend.models.models import VotesmartBills
from backend.models.models import ContactInfo
from backend.models.models import Ratings
from backend.models.models import InterestGroups
from backend.models.models import CandidateFinancialContributions
import os
import requests
import json
import xmltodict
import re


populate_database = Blueprint(
    "populate_database", __name__, template_folder="templates"
)


@populate_database.route("/api/v1/contactInfo/senate", methods=["POST"])
def populate_senate_contact_info():
    base_url = "https://api.propublica.org/congress/v1/116/senate/members.json"
    headers = {"X-API-Key": os.environ["PROPUBLICA_TOKEN"]}
    response = requests.get(url=base_url, headers=headers)
    json_data = json.loads(response.text)
    results = json_data["results"]
    for result in results:
        members = result["members"]
        for member in members:
            if member["votesmart_id"] is not None:
                contact_info = ContactInfo(
                    first_name=member["first_name"],
                    last_name=member["last_name"],
                    party=member["party"],
                    twitter_account=member["twitter_account"],
                    facebook_account=member["facebook_account"],
                    youtube_account=member["youtube_account"],
                    votesmart_id=member["votesmart_id"],
                    crp_id=member["crp_id"],
                    website_url=member["url"],
                    image_url="https://static.votesmart.org/canphoto/"
                    + member["votesmart_id"]
                    + ".jpg",
                )
                db.session.add(contact_info)
            db.session.commit()
    return "", 200


@populate_database.route("/api/v1/contactInfo/senate", methods=["POST"])
def populate_complete_senate_contact_info():
    base_url = "https://api.propublica.org/congress/v1/116/senate/members.json"
    headers = {"X-API-Key": os.environ["PROPUBLICA_TOKEN"]}
    response = requests.get(url=base_url, headers=headers)
    json_data = json.loads(response.text)
    results = json_data["results"]
    for result in results:
        members = result["members"]
        for member in members:
            if member["votesmart_id"] is not None:
                contact_info = ContactInfo(
                    first_name=member["first_name"],
                    last_name=member["last_name"],
                    party=member["party"],
                    twitter_account=member["twitter_account"],
                    facebook_account=member["facebook_account"],
                    youtube_account=member["youtube_account"],
                    votesmart_id=member["votesmart_id"],
                    crp_id=member["crp_id"],
                    website_url=member["url"],
                    image_url="https://static.votesmart.org/canphoto/"
                    + member["votesmart_id"]
                    + ".jpg",
                    state=member["state"],
                    office="Senate",
                )
                db.session.add(contact_info)
            db.session.commit()
    return "", 200


@populate_database.route("/api/v1/contactInfo/house", methods=["POST"])
def populate_members_contact_info():
    base_url = "https://api.propublica.org/congress/v1/116/house/members.json"
    headers = {"X-API-Key": os.environ["PROPUBLICA_TOKEN"]}
    response = requests.get(url=base_url, headers=headers)
    json_data = json.loads(response.text)
    results = json_data["results"]
    for result in results:
        members = result["members"]
        for member in members:
            if member["votesmart_id"] is not None:
                contact_info = ContactInfo(
                    first_name=member["first_name"],
                    last_name=member["last_name"],
                    party=member["party"],
                    district=member["district"],
                    twitter_account=member["twitter_account"],
                    facebook_account=member["facebook_account"],
                    youtube_account=member["youtube_account"],
                    votesmart_id=member["votesmart_id"],
                    crp_id=member["crp_id"],
                    website_url=member["url"],
                    image_url="https://static.votesmart.org/canphoto/"
                    + member["votesmart_id"]
                    + ".jpg",
                )
                db.session.add(contact_info)
            db.session.commit()
    return "", 200


@populate_database.route("/api/v1/contactInfo/house", methods=["POST"])
def populate_members_complete_contact_info():
    base_url = "https://api.propublica.org/congress/v1/116/house/members.json"
    headers = {"X-API-Key": os.environ["PROPUBLICA_TOKEN"]}
    response = requests.get(url=base_url, headers=headers)
    json_data = json.loads(response.text)
    results = json_data["results"]
    for result in results:
        members = result["members"]
        for member in members:
            if member["votesmart_id"] is not None:
                contact_info = ContactInfo(
                    first_name=member["first_name"],
                    last_name=member["last_name"],
                    party=member["party"],
                    district=member["district"],
                    twitter_account=member["twitter_account"],
                    facebook_account=member["facebook_account"],
                    youtube_account=member["youtube_account"],
                    votesmart_id=member["votesmart_id"],
                    crp_id=member["crp_id"],
                    website_url=member["url"],
                    image_url="https://static.votesmart.org/canphoto/"
                    + member["votesmart_id"]
                    + ".jpg",
                    state=member["state"],
                    office="House",
                )
                db.session.add(contact_info)
            db.session.commit()
    return "", 200


# populates database with bills that candidates have voted on, doesn't contain full bill information
@populate_database.route("/api/v1/populateCandidateBills/", methods=["POST"])
def populate_candidate_bills():
    base_url = "http://api.votesmart.org/Votes.getByOfficial?key={}&o=JSON&candidateId={}&year={}".format(
        os.environ["VOTESMART_KEY"], "{}", "{}"
    )
    contact_info_list = db.session.query(ContactInfo).all()
    for contact_info in contact_info_list:
        for year in range(2016, 2020):
            try:

                votesmart_id = contact_info.votesmart_id

                url = base_url.format(votesmart_id, year)
                response = requests.get(url=url)
                count = 0
                while not response.ok and count < 5:
                    response = requests.get(url=url)
                if not response.ok:
                    return (
                        "Bad response after 5 attempts for candidate_id: "
                        "{}, year: {}, ".format(contact_info.votesmart_id, year),
                        400,
                    )

                json_data = json.loads(response.text)
                if "error" in json_data:
                    continue
                bills = json_data["bills"]
                results = bills["bill"]
                for bill_dict in results:
                    # in case category_list is empty/only one, we need to do try/except and
                    # possibly wrap it in a list
                    try:
                        category_list = bill_dict["categories"]["category"]
                        if isinstance(category_list, dict):
                            category_list = [category_list]
                        categories = "_".join(
                            cat["categoryId"] for cat in category_list
                        )
                    except Exception:
                        categories = ""

                    candidate_bill = CandidateBills(
                        bill_id=bill_dict["billId"],
                        bill_number=bill_dict["billNumber"],
                        title=bill_dict["title"],
                        officeId=bill_dict["officeId"],
                        office=bill_dict["office"],
                        actionId=bill_dict["actionId"],
                        stage=bill_dict["stage"],
                        vote=bill_dict["vote"],
                        categories=categories,
                        candidateId=votesmart_id,
                    )
                    db.session.add(candidate_bill)
            except Exception:
                return (
                    "Investigate error for candidate_id: "
                    "{}, year: {}, ".format(contact_info.votesmart_id, year),
                    400,
                )
            # Insert all for one candidate at a time for efficiency, 20+ or so
            db.session.commit()

    return "", 200


# populates the database with all info on a bill that votesmart has
@populate_database.route("/api/v1/populateVotesmartBills/", methods=["POST"])
def populate_bill_votesmart():
    base_url = "http://api.votesmart.org/Votes.getBill?key={}&o=JSON&billId={}".format(
        os.environ["VOTESMART_KEY"], "{}"
    )
    candidate_bills_list = (
        db.session.query(CandidateBills).group_by(CandidateBills.bill_id).all()
    )
    count = 0
    for candidate_bill in candidate_bills_list:
        try:
            bill_id = candidate_bill.bill_id
            url = base_url.format(bill_id)
            response = requests.get(url=url)
            json_data = json.loads(response.text)
            bill_dict = json_data["bill"]
            # in case the dictionaries are empty, we do a try except block to prevent keyError

            try:
                category_list = bill_dict["categories"]["category"]
                if isinstance(category_list, dict):
                    category_list = [category_list]
                categories = "_".join(cat["categoryId"] for cat in category_list)
            except Exception:
                categories = ""

            try:
                sponsor_list = bill_dict["sponsors"]["sponsor"]
                if isinstance(sponsor_list, dict):
                    sponsor_list = [sponsor_list]
                sponsors = "_".join(
                    ".".join((spon["name"], spon["candidateId"], spon["type"]))
                    for spon in sponsor_list
                )
            except Exception:
                sponsors = ""

            try:
                action_list = bill_dict["actions"]["action"]
                if isinstance(action_list, dict):
                    action_list = [action_list]
                action_ids = "_".join(act["actionId"] for act in action_list)
            except Exception:
                action_ids = ""

            try:
                committee_list = bill_dict["committeeSponsors"]["committeeSponsor"]
                if isinstance(committee_list, dict):
                    committee_list = [committee_list]
                committees = "_".join(com["name"] for com in committee_list)
            except Exception:
                committees = ""

            try:
                amend_list = bill_dict["amendments"]["amendment"]
                if isinstance(amend_list, dict):
                    amend_list = [amend_list]
                amendments = "_".join(am["billNumber"] for am in amend_list)
            except Exception:
                amendments = ""

            votesmart_bill = VotesmartBills(
                bill_id=bill_id,
                bill_number=bill_dict["billNumber"],
                title=bill_dict["title"],
                date_introduced=bill_dict["dateIntroduced"],
                bill_type=bill_dict["type"],
                categories=categories,
                sponsors=sponsors,
                committee_sponsors=committees,
                actionIds=action_ids,
            )
            db.session.add(votesmart_bill)
        except Exception:
            return (
                "Investigate error for bill_id: {} to not input duplicate row.".format(
                    bill_id
                ),
                400,
            )
        # Insert all for one candidate at a time for efficiency, 30+ or so
        count += 1
        if count % 20 == 0:
            db.session.commit()

    return "", 200


@populate_database.route(
    "/api/v1/populateCandidateFinancialContributions/", methods=["POST"]
)
def populate_candidate_financial_contributions():
    contact_info_list = ContactInfo.query.all()
    for contact_info in contact_info_list:
        first_name = contact_info.first_name
        last_name = contact_info.last_name
        crp_id = contact_info.crp_id
        base_url = "https://www.opensecrets.org/api/"
        params = {
            "method": "candIndustry",
            "cid": str(crp_id),
            "cycle": "2020",
            "apikey": os.environ["OPENSECRETS_KEY"],
        }
        response = requests.get(url=base_url, params=params)
        results_string = json.dumps(xmltodict.parse(response.text))
        results_dict = json.loads(results_string)
        base_dict = results_dict["response"]
        industries = base_dict["industries"]
        industry = industries["industry"]
        candidate_financial_contributions = CandidateFinancialContributions(
            first_name=first_name,
            last_name=last_name,
            crp_id=crp_id,
            ind1=industry[0]["@industry_name"] + " Total: " + industry[0]["@total"],
            ind2=industry[1]["@industry_name"] + " Total: " + industry[1]["@total"],
            ind3=industry[2]["@industry_name"] + " Total: " + industry[2]["@total"],
            ind4=industry[3]["@industry_name"] + " Total: " + industry[3]["@total"],
            ind5=industry[4]["@industry_name"] + " Total: " + industry[4]["@total"],
        )
        db.session.add(candidate_financial_contributions)
        db.session.commit()
    return "", 200


@populate_database.route("/api/v1/populate_Ratings/", methods=["POST"])
def populate_ratings():
    get_categories_base_url = (
        "http://api.votesmart.org/Rating.getCategories?key={}&o=JSON"
    )
    get_categories_base_url = get_categories_base_url.format(
        os.environ["VOTESMART_KEY"]
    )
    response = requests.get(get_categories_base_url)
    json_data = json.loads(response.text)
    categories = []
    for category in json_data["categories"]["category"]:
        categories.append(category["categoryId"])
    base_url = "http://api.votesmart.org/Rating.getSigList?key={}&categoryId={}&o=JSON"
    sigs = []
    # category_dict = {}
    sig_dict = {}
    i = 1
    for c in categories:
        url = base_url.format(str(c), os.environ["VOTESMART_KEY"])
        response = requests.get(url)
        json_data = json.loads(response.text)
        for sig in json_data["sigs"]["sig"]:
            if type(sig) == str:
                sigs.append(json_data["sigs"]["sig"]["sigId"])
                # category_dict[json_data["sigs"]["sig"]["sigId"]] = c
                sig_dict[json_data["sigs"]["sig"]["sigId"]] = i
            else:
                # category_dict[sig["sigId"]] = c
                sig_dict[sig["sigId"]] = i
                sigs.append(sig["sigId"])
            i += 1
    base_url = "http://api.votesmart.org/Rating.getSigRatings?key={}&sigId={}&o=JSON"
    rating_base_url = (
        "http://api.votesmart.org/Rating.getRating?key={}&ratingId={}&o=JSON"
    )
    for sId in sigs:
        url = base_url.format(os.environ["VOTESMART_KEY"], str(sId))
        response = requests.get(url)
        json_data = json.loads(response.text)
        if "error" in json_data:
            continue
        for ratings in json_data["sigRatings"]["rating"]:
            if type(ratings) == str:
                timespan = json_data["sigRatings"]["rating"]["timespan"]
                rating_name = json_data["sigRatings"]["rating"]["ratingName"]
                url = rating_base_url.format(
                    os.environ["VOTESMART_KEY"],
                    json_data["sigRatings"]["rating"]["ratingId"],
                )
            else:
                timespan = ratings["timespan"]
                rating_name = ratings["ratingName"]
                url = rating_base_url.format(ratings["ratingId"])
            end = timespan.split("-")
            if int(end[-1]) < 2016:
                print("breaking")
                continue
            sig_Id = sId
            sig_votesmart_id = sId

            response = requests.get(url)
            json_rating_data = json.loads(response.text)
            for rating in json_rating_data["rating"]["candidateRating"]:
                if type(rating) == str:
                    candidate_votesmart_id = json_rating_data["rating"][
                        "candidateRating"
                    ]["candidateId"]
                    r = json_rating_data["rating"]["candidateRating"]["rating"]
                else:
                    candidate_votesmart_id = rating["candidateId"]
                    r = rating["rating"]
                rate = Ratings(
                    candidate_votesmart_id=candidate_votesmart_id,
                    sig_id=sig_Id,
                    sig_votesmart_id=sig_votesmart_id,
                    rating=r,
                    timespan=timespan,
                    rating_name=rating_name,
                )
                db.session.add(rate)
        db.session.commit()
    return "", 200


@populate_database.route("/api/v1/populate_SIGS/", methods=["POST"])
def populate_SIGs():
    get_categories_base_url = (
        "http://api.votesmart.org/Rating.getCategories?key={}&o=JSON"
    )
    get_categories_base_url = get_categories_base_url.format(
        os.environ["VOTESMART_KEY"]
    )
    response = requests.get(get_categories_base_url)
    json_data = json.loads(response.text)
    categories = []
    for category in json_data["categories"]["category"]:
        categories.append(category["categoryId"])
    base_url = "http://api.votesmart.org/Rating.getSigList?key={}&categoryId={}&o=JSON"
    sigs = []
    category_dict = {}
    for c in categories:
        url = base_url.format(os.environ["VOTESMART_KEY"], str(c))
        response = requests.get(url)
        json_data = json.loads(response.text)
        for sig in json_data["sigs"]["sig"]:
            if type(sig) == str:
                sigs.append(json_data["sigs"]["sig"]["sigId"])
                category_dict[json_data["sigs"]["sig"]["sigId"]] = c
            else:
                category_dict[sig["sigId"]] = c
                sigs.append(sig["sigId"])
    base_url = "http://api.votesmart.org/Rating.getSig?key={}&sigId={}&o=JSON"
    open_secrets_summary_url = (
        "http://www.opensecrets.org/api/?method=orgSummary&output=json&id={}&apikey={}"
    )
    open_secrets_search_url = (
        "http://www.opensecrets.org/api/?output=json&org={}&method=getOrgs&apikey={}"
    )
    for sId in sigs:
        url = base_url.format(os.environ["VOTESMART_KEY"], str(sId))
        response = requests.get(url)
        votesmart_json_data = json.loads(response.text)
        formatted_org = votesmart_json_data["sig"]["name"].replace(" ", "_")
        formatted_org = formatted_org.replace("#", "")
        url = open_secrets_search_url.format(
            formatted_org, os.environ["OPEN_SECRETS_KEY"]
        )
        search_response = requests.get(url)
        if (
            search_response.text
            == "Resource not found or query was less than three characters"
        ):
            dems = -1
            repubs = -1
            gave_to_pac = -1
            gave_to_cand = -1
            total = -1
        else:
            search_data = json.loads(search_response.text)
            try:
                votesmart_id = search_data["response"]["organization"]["@attributes"][
                    "orgid"
                ]
            except TypeError:
                votesmart_id = search_data["response"]["organization"][0][
                    "@attributes"
                ]["orgid"]
            url = open_secrets_summary_url.format(
                votesmart_id, os.environ["OPEN_SECRETS_KEY"]
            )
            summary_response = requests.get(url)
            summary_data = json.loads(summary_response.text)
            attributes = summary_data["response"]["organization"]["@attributes"]
            dems = attributes["dems"]
            repubs = attributes["repubs"]
            gave_to_pac = attributes["gave_to_pac"]
            gave_to_cand = attributes["gave_to_cand"]
            total = attributes["total"]

        sig = votesmart_json_data["sig"]
        interest_group = InterestGroups(
            votesmart_id=str(sId),
            category_id=category_dict[sId],
            name=sig["name"],
            description=sig["description"],
            address=sig["address"],
            phone=sig["phone1"],
            dems=dems,
            repubs=repubs,
            gave_to_pac=gave_to_pac,
            gave_to_cand=gave_to_cand,
            total=total,
        )
        db.session.add(interest_group)
    db.session.commit()
    return "", 200


@populate_database.route(
    "/api/v1/removeQuotesFromInterestGroupsDescription", methods=["POST"]
)
def remove_quotes_from_interest_groups():
    interest_groups = InterestGroups.query.all()
    for interest_group in interest_groups:
        description = interest_group.description
        interest_group.description = description.strip('"')
        db.session.commit()
    return "", 200


@populate_database.route(
    "/api/v1/addPrimarySponsorColumnToVotesmartBills", methods=["POST"]
)
def add_primary_sponsor_to_interest_groups():
    votesmart_bills = VotesmartBills.query.all()
    for votesmart_bill in votesmart_bills:
        sponsors = votesmart_bill.sponsors

        if sponsors != "":
            sponsors_split_on_underscore = sponsors.split("_")
            primary_sponsor_bulky = re.split("\.\d+\.", sponsors_split_on_underscore[0])
            primary_sponsor_name = primary_sponsor_bulky[0]
            votesmart_bill.primary_sponsor = primary_sponsor_name
            db.session.commit()
    return "", 200
