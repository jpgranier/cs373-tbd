import backend.__init__ as app
from unittest import main, TestCase
import json


class MyTestCase(TestCase):
    app = app.app.test_client()

    def test_get_candidate_info(self):
        response = json.loads(
            self.app.get("/getCandidateInfo?id=3").get_data(as_text=True)
        )
        self.assertEqual(response["first_name"], "John")

    def test_get_candidate_info_2(self):
        response = json.loads(
            self.app.get("/getCandidateInfo?id=4").get_data(as_text=True)
        )
        self.assertEqual(response["last_name"], "Bennet")

    def test_get_votesmart_bill_1(self):
        response = json.loads(
            self.app.get("/getVotesmartBill?bill_id=19976").get_data(as_text=True)
        )
        self.assertEqual(response["title"], "Authorizes Felons to Vote While on Parole")

    def test_get_votesmart_bill_2(self):
        response = json.loads(
            self.app.get("/getVotesmartBill?bill_id=27572").get_data(as_text=True)
        )
        self.assertEqual(response["bill_id"], str(27572))

    def test_get_interest_groups_by_category_id_1(self):
        response = json.loads(
            self.app.get("/getInterestGroupsByCategoryId?category_id=2").get_data(
                as_text=True
            )
        )
        self.assertEqual(len(response), 5)

    def test_get_interest_groups_by_category_id_2(self):
        response = json.loads(
            self.app.get("/getInterestGroupsByCategoryId?category_id=1").get_data(
                as_text=True
            )
        )
        self.assertEqual(len(response), 0)

    def test_get_candidate_bills_by_action_id_1(self):
        response = json.loads(
            self.app.get("/getCandidateBillsByActionId?actionId=71475").get_data(
                as_text=True
            )
        )
        self.assertEqual(type(response), type([]))

    def test_get_candidate_bills_by_action_id_2(self):
        response = json.loads(
            self.app.get("/getCandidateBillsByActionId?actionId=-1").get_data(
                as_text=True
            )
        )
        self.assertEqual(len(response), 0)

    def test_get_contact_info_1(self):
        response = json.loads(
            self.app.get("/getContactInfoByVoteSmartId?votesmart_id=27110").get_data(
                as_text=True
            )
        )
        self.assertEqual(
            response["first_name"] + " " + response["last_name"], "Bernard Sanders"
        )

    def test_get_contact_info_2(self):
        response = json.loads(
            self.app.get("/getContactInfoByVoteSmartId?votesmart_id=180416").get_data(
                as_text=True
            )
        )
        self.assertEqual(response["website_url"], "https://ocasio-cortez.house.gov/")

    def test_get_interest_group_ids_1(self):
        response = json.loads(
            self.app.get("/getInterestGroupIds").get_data(as_text=True)
        )
        self.assertEqual(len(response), 443)

    def test_get_interest_group_ids_2(self):
        response = json.loads(
            self.app.get("/getInterestGroupIds").get_data(as_text=True)
        )
        self.assertEqual(response[0]["sig_id"], 2)

    def test_get_recent_ratings_1(self):
        response = json.loads(
            self.app.get("/getRecentRatings?sig_id=1086").get_data(as_text=True)
        )
        self.assertEqual(len(response), 20)

    def test_get_recent_ratings_2(self):
        response = json.loads(
            self.app.get("/getRecentRatings?sig_id=1704").get_data(as_text=True)
        )
        self.assertEqual(len(response), 0)

    def test_get_ratings_by_candiate_1(self):
        response = json.loads(
            self.app.get("/getRatingsByCandidate?votesmart_id=27110").get_data(
                as_text=True
            )
        )
        self.assertEqual(response[100]["rating"], "100")

    def test_get_ratings_by_candiate_2(self):
        response = json.loads(
            self.app.get("/getRatingsByCandidate?votesmart_id=27110").get_data(
                as_text=True
            )
        )
        self.assertEqual(response[100]["sig_votesmart_id"], 169)

    def test_get_candidate_ids_1(self):
        response = json.loads(self.app.get("/getCandidateIds").get_data(as_text=True))
        self.assertEqual(len(response), 527)

    def test_get_candidate_ids_2(self):
        response = json.loads(self.app.get("/getCandidateIds").get_data(as_text=True))
        self.assertEqual(response[76]["candidate_name"], "Bernard Sanders")

    def test_get_interest_groups_1(self):
        response = json.loads(
            self.app.get("/getInterestGroups?sig_id=3").get_data(as_text=True)
        )
        self.assertEqual(response["name"], "Democrats for Life of America")

    def test_get_interest_groups_2(self):
        response = json.loads(
            self.app.get("/getInterestGroups?sig_id=3").get_data(as_text=True)
        )
        self.assertEqual(type(response["description"]), str)

    def test_get_top_industries_1(self):
        response = json.loads(
            self.app.get("/getTopIndustries?id=77)").get_data(as_text=True)
        )
        self.assertEqual(response["ind1"], "Retired Total: 1084612")

    def test_get_top_industries_2(self):
        response = json.loads(
            self.app.get("/getTopIndustries?id=77)").get_data(as_text=True)
        )
        self.assertEqual(response["ind2"], "Education Total: 420977")

    def test_get_votes_by_candidate_1(self):
        response = json.loads(
            self.app.get("/getVotesByCandidate?votesmart_id=27110").get_data(
                as_text=True
            )
        )
        self.assertEqual(len(response["votes"]), 10)

    def test_get_votes_by_candidate_2(self):
        response = json.loads(
            self.app.get("/getVotesByCandidate?votesmart_id=27110").get_data(
                as_text=True
            )
        )
        self.assertEqual(
            response["votes"][0]["title"],
            "A resolution to provide for related procedures concerning the articles of impeachment against "
            + "Donald John Trump, President of the United States",
        )

    def test_get_bills_by_category_id_1(self):
        response = json.loads(
            self.app.get("/getBillsByCategoryId?category_id=2").get_data(as_text=True)
        )
        self.assertEqual(len(response["bills"]), 17)

    def test_get_bills_by_category_id_2(self):
        response = json.loads(
            self.app.get("/getBillsByCategoryId?category_id=2").get_data(as_text=True)
        )
        self.assertEqual(
            response["bills"][5]["title"],
            "Prohibits Abortion After Fetal Heartbeat Detection",
        )

    def test_get_top_industries_3(self):
        response = json.loads(
            self.app.get("/getTopIndustries?id=2").get_data(as_text=True)
        )
        self.assertEqual(response["ind1"], "Retired Total: 4894956")

    def test_get_top_industries_by_crp_id(self):
        response = json.loads(
            self.app.get("/getTopIndustriesByCrpId?crp_id=N00000528").get_data(
                as_text=True
            )
        )
        self.assertEqual(response["ind1"], "Retired Total: 1084612")

    def test_get_candidate_bills_by_action_id(self):
        response = json.loads(
            self.app.get("/getCandidateBillsByActionId?actionId=70934").get_data(
                as_text=True
            )
        )
        self.assertEqual(response[0]["title"], "No War Against Iran Act")

    def test_get_votes_by_candidate(self):
        response = json.loads(
            self.app.get("/getVotesByCandidate?votesmart_id=155414").get_data(
                as_text=True
            )
        )
        self.assertEqual(len(response["votes"]), 10)

    def test_get_all_votesmart_bill_bill_types(self):
        response = json.loads(
            self.app.get("/getAllVotesmartBillBillTypes").get_data(as_text=True)
        )
        self.assertEqual(len(response), 5)


if __name__ == "__main__":
    main()
