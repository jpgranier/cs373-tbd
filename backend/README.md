Docker Instructions:

cd /backend
docker build -t thefundsdontstop-server:latest .
docker run -d -p 5000:5000 -e DB_HOST=<host address> -e DB_USER=<user name> -e DB_PASSWORD=<password> -e DB_DATABASE=<database name> --name container_name thefundsdontstop-server
Visit localhost:5000
